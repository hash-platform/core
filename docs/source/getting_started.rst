Getting Started with Hash Kern
==============================

This guide shows you how to create some basic resources with hash kern and manage them using the CLI

Let's start by creating the `config.ini` file for the CLI with this content

.. code-block:: ini

    [LocalFile]
    output = hash_test/storage
    organization = hashio
    project = hash

Now create the directory for local hash state storage using this command

.. code-block:: bash

    mkdir -p hash_test/storage

Let's create a simple terraform resource in this directory `resources/terraform`, add these files with the right content

* provider.tf with this content

.. code-block:: terraform

    terraform {
      required_providers {
        random = {
          source  = "hashicorp/random"
          version = "3.4.3"
        }
      }
    }


* main.tf with this code

.. code-block:: terraform

    resource "random_string" "random" {
      length           = 16
      special          = true
      override_special = "/@£$"
    }


* resource.yaml with this content

.. code-block:: yaml

    apiVersion: hash.io/v1alpha1
    kind: Terraform
    metadata:
        name: test
    spec:
        path: .
        match:
        - "*.tf"

Now we need to create an envrionment called `dev`, put a file called `resource.dev.yaml` directly in `resources` directory with this content

.. code-block:: yaml

    apiVersion: hash.io/v1alpha1
    kind: Env
    metadata:
        name: dev
    spec:
        path: .

Now we can try to build this resource with this command

.. code-block:: bash

    haks --env=dev build resources/terraform

If you try to run build twice then the second time wil do nothing as long as you didn't change the code.

For bigger demos you can check this `repository <https://gitlab.com/hash-platform/getting-started-demos>`_
