State module
============

This module exists in the kern package, it defines the State class which represents a state for a single
resource, in hash every resource has its own state so every state object corresponds to a resource and
also a hash of the resource's code files.

============
External API
============

The state module must provide these external APIs:

* State class to represent state, its initializer takes two arguments, the first is the hash and the second
  is the resource's ID both of them must be strings otherwise a StateError is raised.

* set_result method of State class it accepts these arguments: result dictionary, action string and env string
  it sets the result of executing the action on the env, the env may be None and it checks that the result is valid.

* get_result method of State class to return the result of executing an action in an env.

* get_artifacts and get_globals methods which return artifacts and globals for an action in an env.

* get_hash and get_resource_id methods to read the state's hash and resource ID.

* get_envs and get_env_names methods to return the envs dictionary or the names of envs in which an action was ran.

* add_dep, get_dep and get_deps methods to add a new dependency, read a single dependency or read all dependencies.

* set_status and get_status to set and read the status object of the state.

* get_json method to return a string JSON representation of the state.
