Resources Docs
==============

Every Hash resource has its own specs and outputs, here we will add documentation for the actions, specs and outputs of all resources.

=======================
Terraform Resource Docs
=======================

The terraform resource is used to plan and apply terraform configs.

Environment dependency
----------------------

By default the terraform resource doesn't depend on deploying its enviornment, because we can run terraform `apply` without
deploying an enviornment, it is the resource who will probably create the environment for us.

actions
-------
The terraform resource supports three actions:

* build: It runs `terraform plan`, and saves the terraform plan output as an artifact.

* test: It runs `terraform fmt`, and fails if the terraform code is not formatted correctly.

* deploy: It runs `terraform apply`.

specifications
--------------

The terraform resource supports these specs:

* workspace: is used to define the name of terraform workspace to be used, it defaults to the name of the current environment.

* state_backend: is an object which defines the state backend to be used, every state backend object should have a kind key for
  specifying the kind of backend, the only supported and default kind is `gcs`, it should have these sub-keys:

  * bucket_name: The name of GCP bucket to store the state in it.

  * prefix: A prefix for the state backend, it is used like this in init command `-backend-config='prefix={prefix}'`

  * project: The ID of GCP project where the bucket is located, it is used to create the bucket if it doesn't exist.

  * location: This is the location for the GCP bucket, it is needed when creating the bucket, the location can be **EU** or **US**.

* variables: This is an object for the names of terraform variables and their respective values, you can use this to set variable values
  per environment, by overrding this spec in environment's specs.

* plan_timeout: The timeout in seconds for plan command, its default value is 300.

* store_sensitive_outputs_in_secrets: This is a boolean, it controls whether senstitive outputs are saved in state or returned as secrets
  so hash kern will save them in the secrets provider.

* infracost: This is an object used to specify options for using infracost CLI to check the costs for current config, it has these sub-keys:

  * enabled: A boolean value to enable or disable cost checks, it is false by default.

  * maximum: The maximum allowed cost in US dollars, fail the test if the cost is bigger than this value.

  * api_key: The API key used to authenticate to infracost API for cost checks.

* apply_timeout:  The timeout in seconds for apply command, its default value is 300.

* terraform_version: The version of terraform that must be installed to execute actions on the resource, if not set then use the current available terraform binary in PATH.

* terraform_install: Is a boolean option, it is used to install the needed version if the current available version is not the one needed, default is false, if we set `terraform_version`
  and use false for `terraform_install` then the action will fail if current version is not the needed one.

* terraform_check_sum: This is the checksum of the needed terraform version, there are checksums defined in the resource for many terraform versions, however if we want to install
  a new version that is still not supported by the resource then we can provide a checksum value using this option.

outputs
-------

The terraform resource outputs all the defined outputs in terraform config as resource outputs, outputs are only returned for build and deploy actions, the outputs are divided
into two groups, the first one is all the non-sensitive outputs are normal hash resource outputs (globals), and the second group are the senstitive outputs which are stored in
secrets provider, however, if you set `store_sensitive_outputs_in_secrets` to false then all outputs are saved as hash resource outputs (globals) and saved in state.

Every output is actually an object of three keys:

* senstitive: set to true for senstitive outputs.
* type: contains the type of output's value.
* value: is the actual value for the output.

To read the output's value in a hash template use this syntax:

{{ globals.get("Terraform:name", "deploy", "output_name").get("value") }}

This will return the value for an output called `output_name`, which is a result of running deploy action on a Terraform resource with id `Terraform:name`

artifacts
---------
The terraform resource defined only one artifact called `tfplan` of type file, it results from building the resource and it is the actual plan file, it results
from running this command `terraform plan -out=tfplan -no-color -input=false`

=========================
DockerImage Resource Docs
=========================

Environment dependency
----------------------

This resource uses the DockerRegsitry Target in the environment, so it depends on deploying its environment.

actions
-------

This resource implements these actions:

* build: It runs `docker build` command, the image name is built from the spec options: `image_name` and `image_tag`, `image_name` defaults to the resource's name and if `image_tag`
  is not defined then we use the first 6 characters of the hash, and if it is defined then the first 6 characters of the hash are appended at the end with a `-` before them.
  it creates an artifact called `image_file` of type `file` which is the result from `docker save` command.
* test: It runs some tests on the docker resource, first it tries to run `docker build` command, if the `accept_latest` option is set to false, which is the default
  value, then it ensures that the docker file doesn't use the latest tag when fetching other docker images, if no tag is specified then it is treated as latest.
* publish: It uses the DockerRegsitry target to push the docker image to the registry, it also creates an artifact called `image_url` of type `url` which is the URL of pushed image.
* deploy: It uses the `K8STarget` to create a pod with the image, it takes `pod_name` from `pod_name` spec which defaults to resource's name, it takes the port from `expose_port` spec
  and service account from `service_account` spec.

specifications
--------------

The DockerImage uses these specs:

* image_name: This is the name of docker image to push, it defaults to resource's name.
* image_tag: This is the tag of the image, if not defined then the first 6 characters of the hash are used, and if defined then it is used + `-` + first 6 characters of the hash.
* docker_file: This is the name of docker file, it defaults to `Dockerfile`.
* accept_latest: This is a boolean spec, which defaults to false, when set to false the test of the docker resource will fail if the dockerfile pulls docker images with latest or no tag.
* expose_port: This is the port's to expose when deploying the docker image as a pod to kubernetes cluster.
* service_account: This is the name of the service account used for the pod when deploying to k8s cluster.
* pod_name: This is the name of the pod, it defaults to resource's name.

outputs
-------

This resource has no outputs.

artifacts
---------

These are the artifacts created by this resource:

* image_file: This is an artifact of kind `file`, it is created when building the resource, it is the output of `docker save` command.
* image_url: This is an artifact of kind `url`, it is created when publishing the resource, it is the URL of pushed docker image to the registry.
* image_url_digest: This is an artifact of kind `url`, it is created when publishing the resource, it is the URL of pushed docker image to the registry, using digest instead of tag.

=======================
Kustomize Resource Docs
=======================

The kustomize resource is used for generating k8s manifests from kustomize files, and deploying them to kubernetes clusters.

Environment dependency
----------------------

The Kustomize resource uses K8S Target to test and deploy manifests in kubernetes cluster, so it needs to have the environment
deployed before we can test it or deploy it.

actions
-------

It supports three actions:

* build: It generates kubernetes manifests according to the selected overlay using `overlay` spec which defaults to environment's name,
  it also updates the namespace for overlays using the `namespace` spec if `force_namespace` is set to true, the generated manifests file
  is saved as an artifact with name `manifests`
* test: It tests the manifests by trying to run `kustomize build`, if the namespace spec is set, then it tries to create the namespace, then it
  tests the manifests using K8STarget, which runs `kubectl --dry-run=server ...`
* deploy: It applies the manifests to the kubernetes cluster using K8STarget.

specifications
--------------

The Kustomize resource supports these specs:

* kubectl_version: This defines the version for kubectl to use, if not set then the currently installed kubectl binary is used.
* kubectl_install: This is a boolean spec, it defines if we must install kubectl if the current version does not match the one in `kubectl_version`.
* kubectl_check_sum: This can be used to specify a checksum for a version that is still not supported by the resource.
* overlay: This defines the selected overlay when running kustomize build, this should be the name of a directory inside `overlays` directory.
* missing_overlay: This is a string spec which defines behaviour when trying to use an overlay that doesn't exist, it can use one of these values: `use_base`
  this means try to use a directory called `base` as an overlay and throw an error if it doesn't exist, `ignore` it means ignore this error and generate
  an empty manifests file, `throw_error` it will cause the build to fail, if you give any value for `missing_overlay` other than these it will throw an error.
* namespace: This defines the namespace to use for the manifests, its default value is the environment's name.
* force_namespace: This is a boolean spec, when set to true then all generated manifests will use the namespace from the spec, if set to false then only
  resources without a namespace are deployed to the namespace in spec.
* envs: Here we can put an object of environment variables which can be used in the resources to replace them.
* remove_deleted_resources: This is a boolean spec, when set to true then the Kustomize resource will delete from the cluster any resources that were applied
  before but are no longer in the manifests file, the default value is false.

outputs
-------

This resource has no outputs.

artifacts
---------

This resource has one artifact called `manifests`, it results from building the resource and contains the final rendered kubernetes manifests.

============================
GoMicroService Resource Docs
============================

The GoMicroService resource is used to manage a single microservice written in go, it builds and pushes a docker image, applies kubernetes manifests.

Environment dependency
----------------------

This resource uses K8S target and DockerRegsitry target to publish docker images and deploys kubernetes manifests, so it depends on deploying the environment.

actions
-------

It supports three actions:

* build: The build action runs `go mod tidy` in the directory where `go.mod` is located, this is defined using the spec `go_mod_path` which has
  a default value of Resource Space root directory, then it runs `go build` command to build the go binary, which has the name defined in spec `exec_name`
  and it has a default value of resource's name. It also builds and pushes the docker image, generates k8s manifests in the directory called `k8s` using
  the enviornment's name as an overlay. It returns three artifacts: binary executable file, kubernetes manifests file and pushed image URL.
  if the deploy target is set to `DOFunction` then it only builds the binary and returns it as an artifact. When generating manifests you can use
  the enviornment variable called `IMAGE_URL` to replace it with the URL of pushed image.
  If govulncheck is enabled then it will check for vulnerabilities before building the binary and fail if any vulnerabilities were found.
* test: The test action runs go tests, also tests kubernetes manifests and also tests the docker image.
* deploy: The deploy command works according to the value of `deploy_target`, if set to `kubernetes` then it applies kubernetes manifests to the cluster
  and if set to `DOFunction` then it uses the DOFunction target to deploy the microservice to the Digital Ocean functions service.

specifications
--------------

The GoMicroService resource has these specs:

* go_version: This is the version of go compiler to use, if not set then it will use the currently available go binary.
* golangci_lint_version: This is the version of golang ci linter to use, if not set then it will use the currently available golangci-lint binary.
* golangci_lint_checksum: This is the checksum of golangci-lint, you can use it to specify a checksum if the version of golangci-lint that you want to use is
  not supported yet by the resource.
* go_checksum: This is the checksum of go compiler to use, you can use it to specify a checksum if the version of go compiler that you want to use is
  not supported yet by the resource.
* go_install: This is a boolean option, when set to true, then the right version of go compiler is installed and used, but when set to false and the wanted
  go compiler version is differnt than the currently installed one, then the action will fail.
* golangci_lint_install: This is a boolean option, when set to true, then the right version of golangci linter is installed and used, but when set to false and the wanted
  golangci linter version is differnt than the currently installed one, then the action will fail.
* deploy_target: This is used to specify which kind of target to use when deploying the GoMicroService, it has two values `kubernetes` and `DOFunction`, the default
  value is `kubernetes`, if it is set to another value then the action will fail. When it is set to `DOFunction` then the action will not build a docker image or generate
  kubernetes manifests and will only have the executable's binary as an artifact. Also the right target kind must exist in the environment otherwise it will fail.
* exec_name: This is the name of executable to compile, its default value is the resource's name.
* go_mod_path: This is the path where `go.mod` and `go.sum` files are stored, default value is Resource Space root directory.
* namespace: This is the value for the namespace of generated kubernetes manifests, it is only processed when testing the resource, it tries to create the namespace using K8S target.
* do_function: This is the name of function in Digital Ocean, it is only used when `deploy_target` is set to `DOFunction`.
* govulncheck: This is an object for specifying options for the govulncheck tool, it hash these sub-keys:

  * enabled: When set to true then govulncheck is enabled, default is false.

  * version: Is used to select a version for govulncheck to use, it is installed using `go install` command to local hash directory for the resource space, default version is `v0.1.0`.

outputs
-------

This resource has only one output that is called `do_function_url`, it results from deploying the GoMicroService when `deploy_target` is set to `DOFunction`.

artifacts
---------

This resource has only 3 artifacts which result from running build action, these are:

* An artifact called `binary` of kind file, which is the binary file resulted from compiling go code.
* An artifact called `k8s` which is of kind file, it is produced only when `deploy_target` is set to `kubernetes`, it contains the kubernetes manifests.
* An artifact called `image_url` which is of kind url, it is produced only when `deploy_target` is set to `kubernetes`, it contains the URL of pushed docker image.

===================
Istio Resource Docs
===================

The istio resource is used to manage the installation of istio service mesh to kubernetes cluster.

Environment dependency
----------------------

This resource uses K8S target to run `istioctl` commands, so it depends on deploying the environment.

actions
-------

It supports one action:

* deploy: The deploy command uses the K8S Target to run `istioctl install` on the kubernetes cluster.

specifications
--------------

The Istio resource has these specs:

* version: The version of istio to install, default value is 1.17.2.
* checksum: The checksum of the istio version, this checksum is for the istioctl CLI installed from `https://github.com/istio/istio/releases/download/{version}/istioctl-{version}-linux-amd64.tar.gz`,
  you can use it if the version that you want to install is not supported by the resource.
* profile: The name of built-in profile to install, default is default profile.
* config_file: The name of config file to use after the `-f` parameter to `istioctl` install command, default is None.
* revision: The revision used when installing istio, it is passed in `-r` parameter to `istioctl` install command, default is None.

outputs
-------

This resource has no outputs.

artifacts
---------

This resource has no artifacts.
