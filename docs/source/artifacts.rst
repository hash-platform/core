Artifacts
=========

An artifact is the result of executing an action, for example when building a Go program the created binary is
an artifact of type `file`, when pushing a docker image to a docker registry the full image URL is an artifact
of type `url`.

Introduction
------------

In hash every action on a resource may create an artifact and these artifacts needs to be stored in state storage
and retrieved later, how these artifacts are stored and retrieved later is determined only by the storage system and
could be different from one system to another.

Here we will learn how resources create artifacts, how they are represented using the `Artifact` class and how actions
can use artifacts from other actions, the deploy action might need the artifact from `publish` action.

How resources create artifacts?
-------------------------------
A resource can declare artifacts using the `artifacts` key in its returned result, every artifact is an instance of `Artifact`
class which contains all the attributes needed to uniquely identify the artifact.

The artifact object has a `getData` method which is used to get the artifact's data according to its kind, for files it creates a file
and write the content to it an returns the files path, for texts and URLs it retrieves the text and URL as strings.

This code snippet returns three artifacts of the three kinds

.. code-block:: python

    artifacts = [
        self.create_artifact("id1", "build", "test", "hash1" "file", "artifact-file.txt"),
        self.create_artifact("id2", "build", "", "hash1" "url", "https://dev.hashio.io/service1"),
        self.create_artifact("id3", "build", "", "hash1" "text", "test text")
    ]
    ret = {"artifacts": artifacts}

The create_artifact method takes these parameters

* ID: Here we give the artifact a unique ID to be used later when we want to retrieve it.
* Action: the current action which was used to generate the artifact.
* env name: The name of the environment where thsi action was called.
* hash: The used hash to generate this artifact
* kind: The kind of this artifact it could be either, file, text or url.
* data: The actual data for the artifact, it could be a URL, simple text or a path to a file, this depends on the kind.


How are artifacts accessed in templates?
----------------------------------------

Templates can access artifacts using the artifacts object like this

.. code-block:: python

    {{ artifacts.get_artifact("Terraform:test", "build", "id1").getData() }}

The method get_artifact returns an artifact object which has the `getData` method to get the actual artifact's data.
