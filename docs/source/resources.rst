resources package
=================

base module
-----------

.. automodule:: hash.resources.base
  :members:

targets module
--------------

.. automodule:: hash.resources.targets
  :members:
