Installation
============

Install from PyPi
-----------------

Hash kern is published to PyPi `here <https://pypi.org/project/hashkern/>`_, you can install it using this command

.. code-block:: bash

    pip install hashkern

Then you can use the CLI like this

.. code-block:: bash

    > haks
    usage: haks [-h] [--storage STORAGE] [--config CONFIG] [--env ENV] [--plan PLAN] {build,test,publish,deploy,hash,render,clear,version} ...

    A tool to build resources based on their hash and type

    positional arguments:
    {build,test,publish,deploy,hash,render,clear,version}

    optional arguments:
    -h, --help            show this help message and exit
    --storage STORAGE     The storage system used default is Local File
    --config CONFIG       The configuration file default is config.ini
    --env ENV             An environment to run the action in it
    --plan PLAN           Only do a plan and save it to this file

Install from source
-------------------

Install the required packages to clone the code, create virtual env and run the tests with make

.. code-block:: bash

    sudo apt install git make python3 python3-venv python3-apt

Now clone the repository and cd into its directory

.. code-block:: bash

    git clone https://gitlab.com/hash-platform/hashkern.git
    cd hashkern

Create a virtual env and activate it with these commands

.. code-block:: bash

    python3 -m venv venv
    source venv/bin/activate

Now install hash kern in editable mode and all of its dependecnies with these commands

.. code-block:: bash

    pip install -e .
    pip install -r requirements.txt

Now you can use the CLI located at `src/client/main.py`

You can run tests with this command

.. code-block:: bash

    make test
