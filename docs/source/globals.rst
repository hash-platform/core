Globals
=======

Introduction
------------

Globals are values that are exported by resources and are stored in the state storage for retrieval
later, so far they can only be used in hash templates, where we can use their values in the templates.

How does a resource create globals?
-----------------------------------
Any resource can create globals for every action by returning a `globals` key in its result like this:


.. code-block:: python

    result = {
        "globals": {
            "count": 3,
            "name": "test-name"
        }
    }

This will add the `globals` dictionary to the storage, it can be retrieved later.

How are globals stored?
-----------------------
Globals storage depends on the storage plugin, they can be stored in a file for example, or any other storage possible.

How are globals accessed in templates?
--------------------------------------
Hash templates are actually Jinja2 templates, they have an object called `globals` which stores all globals in the project, to use
the value of a global we write this code

.. code-block:: python

    {{ globals.get("Terraform:web", "build", "count") }}

This gets the gloabl output named count, which is the result of running the action build on a resource with the ID **Terraform:web**.

Using this in a template for a resource will automatically add a dependency for the current resource in running action **build** on the
**Terraform:web** resource in the current environment.

Some resources have their own environment defined in their metadata, so we need to always read globals for them in their own environment, to
do this the developer must provide a fourth parameter here for the get method equals to the resource's environment's name.
