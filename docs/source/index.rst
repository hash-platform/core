.. Hash documentation master file, created by
   sphinx-quickstart on Sat Jun 12 12:01:03 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hash Kern documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   getting_started
   storage_plugin_tutorial
   resource_plugin_tutorial
   artifacts
   globals

   resources_guide

   targets_docs
   resources_docs

   state

   hash
   store
   resources


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
