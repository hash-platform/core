Targets Docs
============

Here you can find documentation about all targets defined in Hash Kern, but first let's learn what a target is?

A **Target** is a python class that can be used to execute actions for different resources in different environments, the logic
for executing these actions is shared among multiple resources and environments, that is why we use targets and not code this
logic in the resources or environments themselves, examples of targets are **K8STarget**, **DockerRegistryTarget**, **DOFunctionTarget**, etc..
these can be used to deploy to kubernetes clusters (K8STarget), push to docker registry (DockerRegistryTarget) and deploy
code to Digital Ocean functions service (DOFunctionTarget), as you can see these actions are common among different resources, we
need to deploy manifests to kubernetes cluster from GoMicroService, Kustomize, etc... resources. Instead of duplicating this logic
in multiple resources, we define it using targets and these are attached to environments, to enable us to have different clusters
for different environments.

=============================
Target definition and commons
=============================

A target is defined on the environments' resource using this syntax:

.. code-block:: yaml

    targets:
    - kind: DockerRegistryTarget
      name: docker
      spec:
        registry_url: <docker registry URL>
        docker_config: <docker config to authenticate to registry>
    - kind: K8STarget
      name: k8s
      spec:
        k8s_certificate_ca: <k8s cluster certificate>
        server: <k8s API endpoint>
        token: <User token>
        k8s_kube_config: <raw k8s kube config file>

This snippet shows some targets and their specs, every target has a `kind`, `name` and `spec`, the specs depend on what target kind
do we have, each target has its own specs, here we define the specs which are needed to execute the target against the env, but how
do we tell the K8STarget for example which manifests to apply, here comes a second part of the target and that is its config which is
passed by the resource to the target's action method, which takes two arguments the action's name and the config, the name could be anything
supported by the target but it mostly resembles normal actions such as `build`, `test`, `publish` and `deploy`. The config is different
between targets, for example the K8STarget needs the manifests file that it must apply, the creds to the k8s cluster are defined in the spec
because they are environment bound and not resource bound, we want to deploy all of our resources in an env to the same cluster, right?

Now let's take a deep dive into the K8STarget and learn about its specs and its configs and actions.

==============
K8STarget Docs
==============

The K8STarget is used to test and deploy k8s manifests on kubernetes clusters, it has these specs defined:

* context: This must be a string which contains the name of kubectl context to use.
* kube_config: The name of file which contains the kubectl config, we could also pass the file's content here as an output from a
  terraform resource, then the target will create a temp file for this content and use the file instead.
* k8s: this is a dictionary which contains the kubernetes cluster credentials, these are:

  * certificate_ca: The certificate for the kubernetes CA, it is passed to `--certificate-authority` option, we can also pass the actual cert here and a temp file will be created for us.

  * certificate: The certificate for the kubernetes, it is passed to `--client-certificate` option, we can also pass the actual cert here and a temp file will be created for us.

  * client_key: The client_key used when connecting to kubernetes cluster, it is passed to `--client-key` option, we can also pass the actual key here and a temp file will be created for us.

  * server: The kubernetes API endpoint, it is passed to `--server` option.

  * token: The user token, it is passed to `--token` option.
* gke: This object defines the properties for a GKE cluster to connect to it.

  * cluster_name: The name of GKE cluster to connect to it, it is passed as first argument to `gcloud container clusters get-credentials` command.

  * project_id: The project ID where the cluster is located, it is passed to the `--project` option.

  * region: The region where the cluster is located if it is a regional cluster, it is passed to the `--region` option.

  * zone: The zone where the cluster is located if it is zonal cluster, it is passed to the `--zone` option..
* aks: This object defines the properties for an AKS cluster to connect to it.

  * cluster_name: The name of AKS cluster to connect to it, it is passed `--name` option to `az` command.

  * subscription_id: The subscription ID where the cluster to connect to is located, it is passed `--subscription` option to `az` command.

  * resource_group: The resource_group where the cluster to connect to is located, it is passed `--resource-group` option to `az` command.

That is all for the target's specs, now we need to take a look at the target's config argument.

.. hint:: The targets specs are used by the end user to specify to which cluster he wants to deploy, while target's config is used by resource developers
  to specify what to deploy.

The action's method for K8STarget takes two argument just like all other targets, these are:

* action: The name of the action to execute, this could be either `deploy`, `test`, `create_namespace`, `delete`, `istioctl_install` and `istioctl_verify-install`.
  The `deploy` action applies the kubernetes manifests to the cluster using `kubectl apply` command, the `test` action
  tests the manifests against the cluster using the `kubectl apply --dry-run=server` command, the `create_namespace`
  action is used to create a new namespace using the `kubectl create namespace` command, the `delete` action removes
  a single resource from the cluster by its name, kind and namespace, the `istioctl_install` action installs istio using `istioctl install` command, the `istioctl_verify-install`
  verifies istio is installed using `istioctl verify-install`.
* config: The config is a dictionary with these keys:

  * kubectl: This defines a path to the kubectl binary used for executing commands, it defaults to **kubectl**.

  * path: This defines the path where the commands will be executed, it is mandatory and has no default value.

  * kind: This specifies what kind of config will the target use, it could be either `Kustomize` for applying manifests or `DockerImage` to deploy a docker image to the cluster.
    its default value is `Kustomize`

  * service_account: This defines a servcie account to use when executing kubectl commands, it is passed to the `--serviceaccount` option for kubectl, this is optional.

  * istioctl: This is an instance of `resources.IstioCtl` class which encapsulates istioctl commands for execution.

The rest of the configs depend on the kind, if we have kind set to `Kustomize`, we need to provide these options:

* manifests_path: This is a path to the file with kubernetes manifests, it could be a kustomization file or plain kubernetes manifests file.

* diff: This is a boolean option, if it is set to true then the method will return True if manifests have changed and None otherwise. If the option
  is set to false then it is ignored.

If the kind is set to `DockerImage`, we need these options

* image_url: This is the URL to the image that we want to deploy to the cluster. This option is mandatory.

* pod_name: This is the name of the pod that we want to create. This option is mandatory.

* port: This is the port number that will be exposed, it is passed as `--port` option to kubectl command.

If the action's name is set to `create_namespace`, then we only have one needed option in the config that is `namespace`,
which sets the name of the namespace to create.

If the action is set to `delete`, we need these options:

* resource_kind: The kind of kubernetes resource that we want to delete, this one is mandatory.

* resource_name: The name of kubernetes resource that we want to delete, this one is mandatory.

* resource_namespace: The namespace of kubernetes resource that we want to delete, this one is mandatory, only if the resource kind is not Namespace or a Cluster wide resource.

The delete command is `kubectl delete {resource_kind} {resource_name} -n {resource_namespace}`

If the action is set to `istioctl_install`, we need these options:

* istio: This is an object which defines istio related paramaters such as:

  * profile: The name of istio built-in profile to install, it is passed as `--set profile=<profile>`

  * config_file: The istio configuration file, passed using `-f` paramater.

  * revision: The name of revision used when installing istio, it is passed as `-r` paramater.

The `istioctl_verify-install` action only needs the mentioned `istioctl` config key.

=========================
DockerRegistryTarget Docs
=========================

This target is used to push docker images to a docker registry, it is used by DockerImage and GoMicroService resources to push
their own docker images.

It supports the following spec options:

* registry_url: This is the URL for the docker registry to push to it, this option is mandatory.

* docker: This object holds config for a docker config file, it has these keys:

  * config: When defined this holds the entire docker config, it is saved to a file and later used.

  * credHelpers: When defined it is used to populate `credHelpers` key in docker config file, it is a only used if `docker.config` is not specified.

* gcr: This object holds the config for a google container registry configuration, it has this sub-key

  * service_account: This is used to impersonate an SA when calling `gcloud auth print-access-token --impersonate-service-account` command to get
    an access  token which is used to login to the docker registry in google, with the username `oauth2accesstoken`.
    If you want to use gcr but not a service account, pass an empty value for `service_account`
* acr: This object holds the config for a azure container registry configuration, it has these sub-keys

  * registry_name: This is the name of registry used in `az acr login` command as `--name` option.

  * subscription_id: This is the ID of the subscription used in `az acr login` command as `--subscription` option.

Now for the action method, it has two paramaters

* name: This is the name of the action, `publish` and `inspect` are supported.

* config: It has these sub-keys

  * kind: This defines the kind of image we want to push, this could be either `DockerImage` or `DockerFile`, it defaults to `DockerFile`.

  * path: This defines the path where the commands will be executed, it is mandatory and has no default value.

  * image_name: this defines the name of the image to be pushed along with its tag, this is the part after registry URL, this field is mandatory.

  * docker_file and docker_file_path: These define the name of the Dockerfile to process and where it is located, these are passed to `docker build` command, these are only processed,
    when kind is set to `DockerFile`, the default value for `docker_file` is `Dockerfile` and default value for `docker_file_path` is the same as value for path.

  * image_file: this is only processed when kind is set to `DockerImage`, it points to where the saved docker image is located, so it is loaded, tagged and pushed.

The publish action pushes the docker image to the docker registry, while the inspect action runs `docker manifest inspect` command to get metadata about the image.

When the action is set to `publish`, it returns a dictionary with these two keys:

* image_url: The URL of the pushed image using tag value.
* image_url_digest: The URL of the pushed image using digest value.

When the action is set to `inspect`, it returns a dictionary with these two keys:

* found: This is either true or false, if the image was foun din the dokcer registry or not.
* data: This is the returned JSON data from `docker manifest inspect`, if found is False then data is an empty dictionary.
