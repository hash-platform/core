docs/%:
	make -C docs $*


test_hash:
	pytest -v --cov=hash tests/test_hash.py --durations=5 --cov-report=html

test_utils:
	pytest -v --cov=hash.utils tests/test_utils.py --durations=5 --cov-report=html

test_dag:
	pytest -v tests/test_dag.py --durations=5

test_kern:
	pytest -v --cov=hash.kern tests/kern --durations=5 --cov-report=html


test_action:
	pytest -v --cov=hash.kern.action tests/kern/test_action.py --durations=5 --cov-report=html

test_state:
	pytest -v --cov=hash.kern.state tests/kern/test_state.py --durations=5 --cov-report=html

test_planner:
	pytest -v --cov=hash.kern.planner tests/kern/test_planner.py --durations=5 --cov-report=html

test_execute:
	pytest -v --cov=hash.kern.execute tests/kern/test_execute.py --durations=5 --cov-report=html


test_render:
	pytest -v --cov=hash.kern.templates tests/kern/test_render.py --durations=5 --cov-report=html

test_main:
	pytest -v --cov=hash.kern.main tests/kern/test_main.py --durations=5 --cov-report=html


test_resources:
	pytest -v --cov=hash.resources tests/resources --durations=5 --cov-report=html

test_download:
	pytest -v tests/resources/test_download.py --durations=5

test_local_store:
	pytest -v --cov=hash.store tests/store/test_local_store.py --durations=5 --cov-report=html
test_store:
	pytest -v --cov=hash.store tests/store --durations=5 --cov-report=html


test: test_kern test_resources test_dag test_store test_hash test_utils
