import os
import pytest
from hash import errors
from hash.kern.main import main_action
from hash.resources.base import ResourceSpace

from hash.store.base import HashStoreLocalFile

from common import create_resource, create_local_file_store_config
from hash.utils import ImDict


def test_main_1(tmp_path):
    spec = {"build_result": "hash_build"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(tmp_path).calculate_hash(res)
    main_action(str(tmp_path / "resource.yaml"), "build", None, tmp_path, ls)
    st = ls.get("Fake:fake", h)
    build_result = st.get_result("build")
    assert build_result["build_result"] == "hash_build"


def test_main_2(tmp_path):
    spec1 = {"build_result": "hash_build1"}
    spec2 = {"build_result": "hash_build2"}
    create_resource(tmp_path / "resource1.yaml", "Fake", "fake1", spec1)
    res = create_resource(
        tmp_path / "resource2.yaml",
        "Fake",
        "fake2",
        spec2,
        depends_on=[{"id": "Fake:fake1"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(tmp_path).calculate_hash(res)
    main_action(str(tmp_path / "resource2.yaml"), "build", None, tmp_path, ls)
    st = ls.get("Fake:fake1", h)
    build_result = st.get_result("build")
    assert build_result["build_result"] == "hash_build1"
    st = ls.get("Fake:fake2", h)
    build_result = st.get_result("build")
    assert build_result["build_result"] == "hash_build2"
    st = ls.get("Fake:fake3", h)
    build_result = st.get_result("build")
    assert build_result == ImDict({})


def test_main_3(tmp_path):
    spec = {"build_result": "hash_build"}
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(tmp_path).calculate_hash(res)
    main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    st = ls.get("Fake:fake", h)
    build_result = st.get_result("build", "fake")
    assert build_result["build_result"] == "hash_build"
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert g == []


def test_main_4(tmp_path):
    spec = {"build_result": "hash_build", "test_result": "hash_test"}
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(tmp_path).calculate_hash(res)
    main_action(str(res_dir / "resource.yaml"), "test", "fake", tmp_path, ls)
    st = ls.get("Fake:fake", h)
    build_result = st.get_result("build", "fake")
    assert build_result["build_result"] == "hash_build"
    test_result = st.get_result("test", "fake")
    assert test_result["test_result"] == "hash_test"
    g = main_action(str(res_dir / "resource.yaml"), "test", "fake", tmp_path, ls)
    assert g == []


def test_main_5(tmp_path):
    spec = {"build_result": "hash_build", "test_result": "hash_test"}
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(tmp_path).calculate_hash(res)
    main_action(str(res_dir / "resource.yaml"), "test", None, tmp_path, ls)
    st = ls.get("Fake:fake", h)
    build_result = st.get_result("build", None)
    assert build_result["build_result"] == "hash_build"
    test_result = st.get_result("test", None)
    assert test_result["test_result"] == "hash_test"
    g = main_action(str(res_dir / "resource.yaml"), "test", None, tmp_path, ls)
    assert g == []


def test_main_6(tmp_path):
    spec = {
        "build_result": "hash_build",
        "artifacts": [
            {
                "id": "af1",
                "kind": "file",
                "content": "af test1",
                "path": str(tmp_path / "af.txt"),
            }
        ],
    }
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "dev")
    create_resource(res_dir / "resource.prod.yaml", "Env", "prod")
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    main_action(str(res_dir / "resource.yaml"), "build", "dev", str(tmp_path), ls)
    st = ls.get("Fake:fake", h)
    build_result = st.get_result("build", "dev")
    assert build_result["build_result"] == "hash_build"
    main_action(str(res_dir / "resource.yaml"), "build", "prod", str(tmp_path), ls)
    st = ls.get("Fake:fake", h)
    build_result = st.get_result("build", "dev")
    assert build_result["build_result"] == "hash_build"


def test_main_7(tmp_path):
    spec = {"globals": {"x": 1}}
    spec2 = {"build_result": "$Fake:fake.build.x"}
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    res1 = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    res = create_resource(res_dir / "resource.2.yaml", "Fake", "fake2", spec2)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert len(g) == 5
    _globals = ls.get_globals()
    assert _globals["Fake:fake"]["fake"]["build"]["x"] == 1
    g = main_action(str(res_dir / "resource.2.yaml"), "build", "fake", tmp_path, ls)
    assert len(g) == 1
    h = ResourceSpace(tmp_path).calculate_hash(res)
    st = ls.get("Fake:fake2", h)
    build_result = st.get_result("build", "fake")
    assert build_result["build_result"] == 1
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert g == []
    g = main_action(str(res_dir / "resource.2.yaml"), "build", "fake", tmp_path, ls)
    assert g == []


def test_main_8(tmp_path):
    spec = {
        "artifacts": [
            {"id": "test", "kind": "file", "path": "hello", "content": "hello, test"}
        ]
    }
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert len(g) == 5
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    st = ls.get("Fake:fake", h)
    assert len(st.get_artifacts("build", "fake")) == 1
    assert len(g) == 0


def test_main_9(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    create_resource(res_dir / "resource.fake.yaml", "Fake", "fake2")
    res = create_resource(
        res_dir / "resource.yaml", "Fake", "fake", depends_on=[{"id": "Fake:fake2"}]
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert len(g) == 6
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert len(g) == 0


def test_main_10(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    create_resource(res_dir / "resource.fake.yaml", "Fake", "fake2")
    res = create_resource(
        res_dir / "resource.yaml", "Fake", "fake", depends_on=[{"id": "Fake:fake2"}]
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    g = main_action(str(res_dir / "resource.yaml"), "test", "fake", tmp_path, ls)
    assert len(g) == 8
    g = main_action(str(res_dir / "resource.yaml"), "test", "fake", tmp_path, ls)
    assert len(g) == 0


def test_main_11(tmp_path):
    spec = {
        "artifacts": [
            {"id": "test", "kind": "file", "path": "hello", "content": "hello, test"}
        ]
    }
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert len(g) == 5
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    assert len(g) == 0
    g = main_action(str(res_dir / "resource.yaml"), "build", "fake", tmp_path, ls)
    assert len(g) == 0


def test_main_12(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "fake")
    res2 = create_resource(res_dir / "resource.fake.yaml", "Fake", "fake2")
    res = create_resource(
        res_dir / "resource.yaml", "Fake", "fake", depends_on=[{"id": "Fake:fake2"}]
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    g = main_action(str(res_dir / "resource.yaml"), "test", "fake", tmp_path, ls)
    assert len(g) == 8
    g = main_action(str(res_dir / "resource.yaml"), "test", "fake", tmp_path, ls)
    assert len(g) == 0
    st = ls.get("Fake:fake", h)
    assert st.get_dep("Fake:fake2") == ResourceSpace(str(tmp_path)).calculate_hash(res2)


def test_main_13(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "dev")
    create_resource(res_dir / "resource.staging.yaml", "Env", "staging")
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", env="dev")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    g = main_action(str(res_dir / "resource.yaml"), "build", "staging", tmp_path, ls)
    assert len(g) == 5
    assert g[4].get("env") == "Env:dev"
    g = main_action(str(res_dir / "resource.yaml"), "build", "staging", tmp_path, ls)
    assert len(g) == 0


def test_main_14(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.dev.yaml", "Env", "dev")
    create_resource(
        res_dir / "resource.staging.yaml",
        "Env",
        "staging",
        depends_on=[
            {"id": "Fake:fake", "action2": "deploy"},
            {"id": "Fake:fake2", "action2": "build"},
        ],
    )
    res = create_resource(res_dir / "resource.fake.yaml", "Fake", "fake", env="dev")
    create_resource(res_dir / "resource.fake2.yaml", "Fake", "fake2")
    create_resource(res_dir / "resource.fake3.yaml", "Fake", "fake3")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    g = main_action(
        str(res_dir / "resource.fake3.yaml"), "build", "staging", tmp_path, ls
    )
    assert len(g) == 14
    assert g[1].get("env") == "Env:staging"
    assert g[5].get("env") == "Env:dev"
    g = main_action(
        str(res_dir / "resource.fake3.yaml"), "build", "staging", tmp_path, ls
    )
    assert len(g) == 0


def test_main_15(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.fake.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    with pytest.raises(errors.ResourceError):
        main_action(
            str(res_dir / "resource.fake.yaml"), "build", "staging", tmp_path, ls
        )


def test_main_16(tmp_path):
    env_dir = tmp_path / "env"
    res_dir = env_dir / "res"
    env_dir.mkdir()
    res_dir.mkdir()
    env_res = create_resource(env_dir / "resource.dev.yaml", "Env", "dev")
    res = create_resource(res_dir / "resource.fake.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    sp = ResourceSpace(str(res_dir))
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 5
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:dev"] == sp.calculate_hash(env_res)
    assert g[1].get("env") == "Env:dev"
    env_res = create_resource(
        env_dir / "resource.dev.yaml", "Env", "dev", spec={"x": "y"}
    )
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 5
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:dev"] == sp.calculate_hash(env_res)


def test_main_17(tmp_path):
    env_dir = tmp_path / "env"
    res_dir = env_dir / "res"
    env_dir.mkdir()
    res_dir.mkdir()
    env_res = create_resource(env_dir / "resource.dev.yaml", "Env", "dev")
    res = create_resource(
        res_dir / "resource.fake.yaml", "Fake", "fake", spec={"depends_on_env": False}
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    sp = ResourceSpace(str(res_dir))
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 1
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:dev"] == sp.calculate_hash(env_res)
    assert g[0].get("env") == "Env:dev"
    env_res = create_resource(
        env_dir / "resource.dev.yaml", "Env", "dev", spec={"x": "y"}
    )
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 1
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:dev"] == sp.calculate_hash(env_res)


def test_main_18(tmp_path):
    env_dir = tmp_path / "env"
    res_dir = env_dir / "res"
    env_dir.mkdir()
    res_dir.mkdir()
    create_resource(env_dir / "resource.dev.yaml", "Env", "dev")
    env_res = create_resource(env_dir / "resource.shared.yaml", "Env", "shared")
    res = create_resource(
        res_dir / "resource.fake.yaml",
        "Fake",
        "fake",
        spec={"depends_on_env": False},
        env="shared",
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    sp = ResourceSpace(str(res_dir))
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 1
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:shared"] == sp.calculate_hash(env_res)
    assert g[0].get("env") == "Env:shared"
    env_res = create_resource(
        env_dir / "resource.shared.yaml", "Env", "shared", spec={"x": "y"}
    )
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 1
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:shared"] == sp.calculate_hash(env_res)


def test_main_19(tmp_path):
    env_dir = tmp_path / "env"
    res_dir = env_dir / "res"
    env_dir.mkdir()
    res_dir.mkdir()
    create_resource(env_dir / "resource.dev.yaml", "Env", "dev")
    env_res = create_resource(env_dir / "resource.shared.yaml", "Env", "shared")
    res = create_resource(
        res_dir / "resource.fake.yaml",
        "Fake",
        "fake",
        spec={"depends_on_env": False},
        env="shared",
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    sp = ResourceSpace(str(res_dir))
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 1
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:shared"] == sp.calculate_hash(env_res)
    assert g[0].get("env") == "Env:shared"
    create_resource(env_dir / "resource.dev.yaml", "Env", "dev", spec={"x": "y"})
    g = main_action(str(res_dir / "resource.fake.yaml"), "build", "dev", tmp_path, ls)
    assert len(g) == 0
    st = ls.get("Fake:fake", sp.calculate_hash(res))
    assert st.get_deps()["Env:shared"] == sp.calculate_hash(env_res)


def test_main_20(tmp_path):
    spec = {
        "artifacts": [
            {
                "id": "test",
                "kind": "file",
                "path": "hello.txt",
            }
        ]
    }
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    with open(res_dir / "hello.txt", "w") as f:
        f.write("test.txt")
    os.chmod(res_dir / "hello.txt", 33277)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(str(tmp_path)).calculate_hash(res)
    g = main_action(str(res_dir / "resource.yaml"), "build", None, tmp_path, ls)
    assert len(g) == 1
    ls.get("Fake:fake", h)
    with open(res_dir / "hello.txt", "r") as f:
        f.read() == "test.txt"
    assert os.stat(res_dir / "hello.txt").st_mode == 33277


def test_main_21(tmp_path):
    spec = {"local": {"x": 1}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    h = ResourceSpace(tmp_path).calculate_hash(res)
    main_action(str(tmp_path / "resource.yaml"), "build", None, tmp_path, ls)
    st = ls.get("Fake:fake", h)
    build_result = st.get_result_local("build")
    assert build_result["x"] == 1
    assert st.get_result_local("build", "__kern").get("__no_env", {}).get("hash") == h
