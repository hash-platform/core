import os
import stat
import pytest

from hash import utils, errors
from hash.kern import action
from common import create_resource


def test_wrong_action_type(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    with pytest.raises(errors.ActionError):
        action.Action(1, res, utils.ImDict({}), None, tmp_path, "").run_action()


def test_wrong_env_type(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    with pytest.raises(errors.ActionError):
        action.Action("build", res, utils.ImDict({}), 1, tmp_path, "").run_action()


def test_wrong_action(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    with pytest.raises(errors.ActionError):
        action.Action("action", res, utils.ImDict({}), None, tmp_path, "").run_action()


def test_setup_env1(tmp_path):
    spec = {"x": "y", "z": 1, "b": False, "l": [1, 2], "d": {"x": 1, "d": [0, 2]}}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", spec, parent="Fake:hash"
    )
    ac = action.Action("build", res, utils.ImDict({}), None, str(tmp_path), "")
    ac.setup_env()
    assert os.environ["R_NAME"] == "fake"
    assert os.environ["R_PARENT"] == "Fake:hash"
    assert os.environ["R_PARENT_NAME"] == "hash"
    assert os.environ["R_ENV"] == ""
    assert os.environ["R_ACTION"] == "build"
    assert os.environ["R_SPEC_X"] == "y"
    assert os.environ["R_SPEC_Z"] == "1"
    assert os.environ["R_SPEC_B"] == "False"
    assert os.environ["R_SPEC_L"] == "(1, 2)"
    assert os.environ["R_SPEC_D"] == "{'d': [0, 2], 'x': 1}"


def test_setup_env2(tmp_path):
    env = create_resource(tmp_path / "resource.dev.yaml", "Env", "dev")
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    ac = action.Action("build", res, utils.ImDict({}), env, str(tmp_path), "")
    ac.setup_env()
    assert os.environ["R_PARENT"] == ""
    assert os.environ["R_PARENT_NAME"] == ""
    assert os.environ["R_ENV"] == "dev"


def test_pre_action1(tmp_path):
    spec = {"pre_build_command": "echo -n pre_build_command > pre.build.txt"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_pre_action()
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "pre_build_command"


def test_pre_action2(tmp_path):
    spec = {"pre_build_command": "echo -n ${R_NAME} > pre.build.txt"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_pre_action()
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "fake"


def test_pre_action3(tmp_path):
    spec = {"pre_build_command": "echo -n ${R_PARENT_NAME} > pre.build.txt"}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", spec, parent="Fake:hash"
    )
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_pre_action()
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "hash"


def test_pre_action4(tmp_path):
    spec = {"pre_build_script": "test.sh", "x": "hash"}
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
echo -n $R_SPEC_X > pre.build.txt
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_pre_action()
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "hash"


def test_action1(tmp_path):
    spec = {"pre_build_script": "test.sh", "x": "hash", "build_result": "a"}
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
echo -n $R_SPEC_X > pre.build.txt
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    r = ac.run_action()
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "hash"
    assert r["build_result"] == "a"


def test_action2(tmp_path):
    spec = {
        "pre_build_command": "echo -n hash > pre.build.txt",
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    r = ac.run_action()
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "hash"


def test_action3(tmp_path):
    spec = {
        "build_command": "echo -n hash > build.txt",
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    r = ac.run_action()
    with open(tmp_path / "build.txt") as f:
        assert f.read() == "hash"


def test_action4(tmp_path):
    spec = {
        "build_command": "echo -n hash > build.txt",
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("test", res, utils.ImDict({}), None, tmp_path, "")
    r = ac.run_action()
    with pytest.raises(FileNotFoundError):
        open(tmp_path / "build.txt")
    assert r["test_result"] == None


def test_action5(tmp_path):
    spec = {"build_script": "test.sh", "x": "hash"}
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
echo -n $R_SPEC_X > build.txt
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    r = ac.run_action()
    with open(tmp_path / "build.txt") as f:
        assert f.read() == "hash"


def test_action6(tmp_path):
    spec = {"build_result": "a"}
    env = create_resource(tmp_path / "resource.dev.yaml", "Env", "dev", {})
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), env, tmp_path, "")
    r = ac.run_action()
    assert r["build_result"] == "a"


def test_action7(tmp_path):
    spec = {"post_build_script": "test.sh", "x": "hash", "build_result": "a"}
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
echo -n $R_SPEC_X > post.build.txt
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    r = ac.run_action()
    with open(tmp_path / "post.build.txt") as f:
        assert f.read() == "hash"
    assert r["build_result"] == "a"


def test_action8(tmp_path):
    spec = {
        "post_build_command": "echo -n hash > post.build.txt",
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    r = ac.run_action()
    with open(tmp_path / "post.build.txt") as f:
        assert f.read() == "hash"


def test_post_action1(tmp_path):
    spec = {"post_build_command": "echo -n post_build_command > post.build.txt"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_post_action()
    with open(tmp_path / "post.build.txt") as f:
        assert f.read() == "post_build_command"


def test_post_action2(tmp_path):
    spec = {"post_build_command": "echo -n ${R_NAME} > post.build.txt"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_post_action()
    with open(tmp_path / "post.build.txt") as f:
        assert f.read() == "fake"


def test_post_action3(tmp_path):
    spec = {"post_build_command": "echo -n ${R_PARENT_NAME} > post.build.txt"}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", spec, parent="Fake:hash"
    )
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_post_action()
    with open(tmp_path / "post.build.txt") as f:
        assert f.read() == "hash"


def test_post_action4(tmp_path):
    spec = {"post_build_script": "test.sh", "x": "hash"}
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
echo -n $R_SPEC_X > post.build.txt
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    ac.do_post_action()
    with open(tmp_path / "post.build.txt") as f:
        assert f.read() == "hash"


def test_action_wrong_ret(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {"ret": 1})
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    with pytest.raises(errors.ResourceBuildError):
        ac.run_action()


def test_action_raise_error(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {"ret": 1})
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    with pytest.raises(errors.ResourceBuildError):
        ac.raise_error("")
    ac = action.Action("test", res, utils.ImDict({}), None, tmp_path, "")
    with pytest.raises(errors.ResourceTestError):
        ac.raise_error("")
    ac = action.Action("publish", res, utils.ImDict({}), None, tmp_path, "")
    with pytest.raises(errors.ResourcePublishError):
        ac.raise_error("")
    ac = action.Action("deploy", res, utils.ImDict({}), None, tmp_path, "")
    with pytest.raises(errors.ResourceDeployError):
        ac.raise_error("")


def test_action_re_action1(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == False


def test_action_re_action2(tmp_path):
    spec = {"re_build_command": "exit 1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == True


def test_action_re_action3(tmp_path):
    spec = {"re_build_command": "exit 2"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == False


def test_action_re_action4(tmp_path):
    spec = {"re_build_command": "exit 0"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == False


def test_action_re_action5(tmp_path):
    spec = {
        "re_build_script": "test.sh",
    }
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
exit 0
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == False


def test_action_re_action6(tmp_path):
    spec = {
        "re_build_script": "test.sh",
    }
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
exit 1
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == True


def test_action_re_action7(tmp_path):
    spec = {
        "re_build_script": "test.sh",
    }
    script_file = tmp_path / "test.sh"
    script_file.write_text(
        """#!/bin/bash
exit 2
    """
    )
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    st = os.stat(str(script_file))
    os.chmod(script_file, st.st_mode | stat.S_IEXEC)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == False


def test_action_re_action8(tmp_path):
    spec = {
        "re_build": True,
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    ac = action.Action("build", res, utils.ImDict({}), None, tmp_path, "")
    assert ac.re_action() == True
