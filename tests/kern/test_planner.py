import pytest
import os
from hash.kern.planner import Planner
from hash.kern.state import State
from hash.resources.base import ResourceSpace
from hash.store.base import HashStoreLocalFile
from hash import dag
from hash import errors, utils

from common import create_resource, create_local_file_store_config

# Read a graph from a file


def read_graph(file):
    d = dag.Graph()
    ret_nodes = []
    with open(file, "r") as f:
        line = f.readline()
        while line:
            nodes = line.split(" ")
            if len(nodes) != 2:
                continue
            n1 = dag.Node(nodes[0], {})
            if n1 not in ret_nodes:
                ret_nodes.append(n1)
            other_nodes = nodes[1].split(",")
            for node in other_nodes:
                other_node = dag.Node(node.strip(), {})
                if other_node not in ret_nodes:
                    ret_nodes.append(other_node)
                e = dag.Edge(n1, other_node)
                d.addEdge(e)
            line = f.readline()
    return d, ret_nodes


base_path = os.path.dirname(__file__)


def test_re_action4(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    pl = Planner(ls, str(tmp_path))
    with pytest.raises(errors.ActionError):
        pl.re_action("build", res, "", "")


def test_re_action5(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    pl = Planner(ls, str(tmp_path))
    assert (
        pl.re_action("build", res, utils.ImDict({"hash": "123"}), "asd") == "new Hash"
    )


def test_re_action6(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    pl = Planner(ls, str(tmp_path))
    assert (
        pl.re_action("build", res, utils.ImDict({"hash": "123"}), "123")
        == "new action in this env: None"
    )


def test_re_action7(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    env = create_resource(tmp_path / "resource.fake.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    assert (
        pl.re_action(
            "build", res, utils.ImDict({"hash": "123"}), "asd", environment=env
        )
        == "new Hash"
    )


def test_re_action8(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    env = create_resource(tmp_path / "resource.fake.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    st = {"hash": "asd", "envs": {"fake": {"build": {"x": 1}}}}
    assert pl.re_action("build", res, utils.ImDict(st), "asd", environment=env) == None


def test_re_action9(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    st = {"hash": "asd", "no_env": {"build": {"x": 1}}}
    assert pl.re_action("build", res, utils.ImDict(st), "asd") == None


def test_re_action10(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    st = {"hash": "asd", "no_env": {"build": {"x": 1}}}
    with pytest.raises(errors.ActionError):
        pl.re_action("build", res, utils.ImDict(st), "asd", ["d:a"])


def test_re_action11(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    st = {"hash": "asd", "no_env": {"build": {"x": 1}}}
    deps = {"x": "a"}
    assert (
        pl.re_action("build", res, utils.ImDict(st), "asd", utils.ImDict(deps))
        == "new deps"
    )


def test_re_action12(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    st = {"hash": "asd", "no_env": {"build": {"x": 1}}, "deps": {"x": "aa"}}
    deps = {"x": "a"}
    assert (
        pl.re_action("build", res, utils.ImDict(st), "asd", utils.ImDict(deps))
        == "dep x changed"
    )


def test_re_action13(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    st = {"hash": "asd", "no_env": {"build": {"x": 1}}, "deps": {"x": "aa"}}
    deps = {"x": "aa"}
    assert (
        pl.re_action("build", res, utils.ImDict(st), "asd", utils.ImDict(deps)) == None
    )


def test_re_action14(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    st = {"hash": "asd", "no_env": {"build": {"x": 1}}, "deps": {"xx": "aa"}}
    deps = {"x": "aa"}
    assert (
        pl.re_action("build", res, utils.ImDict(st), "asd", utils.ImDict(deps))
        == "new dep: x"
    )


def test_planner_get_deps_metadata_wrong_dep_id(tmp_path):
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", depends_on=[{"id": "no"}]
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    with pytest.raises(errors.ResourceError):
        pl.get_deps_metadata(res)


def test_planner_get_deps_metadata_no_dep_id(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", depends_on=[{}])
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    with pytest.raises(errors.ResourceError):
        pl.get_deps_metadata(res)


def test_planner_get_deps_metadata_wrong_dep_list(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", depends_on="a")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    with pytest.raises(errors.ResourceError):
        pl.get_deps_metadata(res)


def test_planner_get_deps_metadata_wrong_dep_object(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", depends_on=["a"])
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    with pytest.raises(errors.ResourceError):
        pl.get_deps_metadata(res)


def test_planner_get_deps_metadata_not_found_dep(tmp_path):
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", depends_on=[{"id": "Fake:hash"}]
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    with pytest.raises(errors.ResourceError):
        pl.get_deps_metadata(res)


def test_planner_get_deps_metadata_no_error(tmp_path):
    res1 = create_resource(
        tmp_path / "resource.hash.yaml",
        "Fake",
        "hash",
    )
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", depends_on=[{"id": "Fake:hash"}]
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps_metadata(res)
    assert len(deps) == 1
    space = ResourceSpace(str(tmp_path))
    h = space.calculate_hash(res1)
    assert deps[0]["hash"] == h


def test_planner_get_deps_metadata_no_deps(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps_metadata(res)
    assert len(deps) == 0
    assert deps == []


def test_planner_get_deps_spec_no_deps(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps_specs(res)
    assert len(deps) == 0
    assert deps == []


def test_planner_get_deps_spec_1_dep(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    spec = {"x": "$Fake:hash.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec=spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps_specs(res)
    assert len(deps) == 1
    space = ResourceSpace(str(tmp_path))
    h = space.calculate_hash(res1)
    assert deps[0]["hash"] == h


def test_planner_get_deps_spec_1_dep_in_key(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    spec = {"$Fake:hash.build.x": "x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec=spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps_specs(res)
    assert len(deps) == 1
    space = ResourceSpace(str(tmp_path))
    h = space.calculate_hash(res1)
    assert deps[0]["hash"] == h


def test_planner_get_deps_spec_2_deps(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res2 = create_resource(tmp_path / "resource.hash2.yaml", "Fake", "hash2")
    spec = {"x": "$Fake:hash.build.x", "t": {"a": "$Fake:hash2.build.a"}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec=spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps_specs(res)
    assert len(deps) == 2
    space = ResourceSpace(str(tmp_path))
    h = space.calculate_hash(res1)
    assert deps[0]["hash"] == h
    h = space.calculate_hash(res2)
    assert deps[1]["hash"] == h


def test_get_deps_none(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    assert deps == []


def test_get_deps_metadata(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        depends_on=[{"id": "Fake:hash", "action2": "test"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert deps[0]["hash"] == h
    assert deps[0]["action2"] == "test"
    assert deps[0]["action1"] is None


def test_get_deps_metadata_double(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        depends_on=[{"id": "Fake:hash"}, {"id": "Fake:hash"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 1
    assert deps[0]["hash"] == h


def test_get_deps_spec(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    spec = {"x": "$Fake:hash.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec=spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 1
    assert deps[0]["hash"] == h


def test_get_deps_spec_metadata(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res2 = create_resource(tmp_path / "resource.hash2.yaml", "Fake", "hash2")
    spec = {"x": "$Fake:hash.build.x"}
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        spec=spec,
        depends_on=[{"id": "Fake:hash2"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 2
    assert deps[0]["hash"] == h
    assert deps[1]["hash"] == space.calculate_hash(res2)


def test_get_deps_spec_metadata_double(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    spec = {"x": "$Fake:hash.build.x"}
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        spec=spec,
        depends_on=[{"id": "Fake:hash"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 2
    assert deps[0]["hash"] == h


def test_get_deps_spec_metadata_double2(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res2 = create_resource(tmp_path / "resource.hash2.yaml", "Fake", "hash2")
    spec = {"x": "$Fake:hash.build.x", "y": "$Fake:hash2.build.x"}
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        spec=spec,
        depends_on=[{"id": "Fake:hash"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 3
    assert deps[0]["hash"] == h
    assert deps[1]["hash"] == space.calculate_hash(res2)


def test_get_deps_spec_metadata_double3(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res2 = create_resource(tmp_path / "resource.hash2.yaml", "Fake", "hash2")
    spec = {
        "x": "$Fake:hash.build.x",
    }
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        spec=spec,
        depends_on=[{"id": "Fake:hash"}, {"id": "Fake:hash2"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 3
    assert deps[0]["hash"] == h
    assert deps[1]["hash"] == space.calculate_hash(res2)


def test_get_deps_spec_metadata_double4(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res2 = create_resource(tmp_path / "resource.hash2.yaml", "Fake", "hash2")
    spec = {
        "x": "$Fake:hash.build.x",
        "y": "$Fake:hash2.build.x",
    }
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        spec=spec,
        depends_on=[{"id": "Fake:hash"}, {"id": "Fake:hash2"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 4
    assert deps[0]["hash"] == h
    assert deps[1]["hash"] == space.calculate_hash(res2)


def test_get_deps_templates(tmp_path):
    res1 = create_resource(tmp_path / "resource.hash.yaml", "Fake", "hash")
    res2 = create_resource(tmp_path / "resource.hash2.yaml", "Fake", "hash2")
    res3 = create_resource(tmp_path / "resource.fake2.yaml", "Fake", "fake2")
    spec = {
        "x": "$Fake:hash.build.x",
        "y": "$Fake:hash2.build.x",
    }
    res = create_resource(
        tmp_path / "resource.yaml",
        "Fake",
        "fake",
        spec=spec,
        depends_on=[{"id": "Fake:hash"}, {"id": "Fake:hash2"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("{{ globals.get('Fake:fake2', 'build', 'x') }}")
    pl = Planner(ls, str(tmp_path))
    deps = pl.get_deps(res)
    space = ResourceSpace(tmp_path)
    h = space.calculate_hash(res1)
    assert len(deps) == 5
    assert deps[0]["hash"] == h
    assert deps[1]["hash"] == space.calculate_hash(res2)
    assert deps[2]["hash"] == space.calculate_hash(res3)
    assert deps[2]["action2"] == "build"


def test_planAction_1(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    state = State("a", "b")
    pl.planAction("build", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    assert len(g.getNodes()) == 1
    assert g.getNodes()[0] == dag.Node("build:Fake:fake:None:a", {})


def test_planAction_2(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    state = State("a", "b")
    pl.planAction("test", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_planAction_2.graph"))
    assert g == g2


def test_planAction_3(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    state = State("a", "b")
    pl.planAction("publish", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_planAction_3.graph"))
    assert g == g2


def test_planAction_4(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    state = State("a", "b")
    pl.planAction("deploy", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_planAction_4.graph"))
    assert g == g2


def test_planAction_5(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    state.set_result({}, "build")
    pl.planAction("deploy", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_planAction_5.graph"))
    assert g == g2


def test_planAction_6(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    state.set_result({}, "build")
    state.set_result({}, "test")
    pl.planAction("deploy", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_planAction_6.graph"))
    assert g == g2


def test_planAction_7(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    state.set_result({}, "build")
    state.set_result({}, "test")
    state.set_result({}, "publish")
    pl.planAction("deploy", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    assert len(g.getNodes()) == 1
    assert g.getNodes()[0] == dag.Node(
        "deploy:Fake:fake:None:b8bbd3f3f497d9c7eff546a51f5a5a1ab053a29560d3daa6bff5d652ff36e09f",
        {},
    )


def test_planAction_8(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    state.set_result({}, "build")
    state.set_result({}, "test")
    state.set_result({}, "publish")
    state.set_result({}, "deploy")
    pl.planAction("deploy", res, None, state.get_imdict(), [])
    g = pl.getGraph()
    assert len(g.getNodes()) == 0


# TODO: Re-enable this test later, now the templates are cleared by planAction
# def test_planAction_9(tmp_path):
#    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
#    output_dir = tmp_path / "output"
#    config = create_local_file_store_config(output_dir)
#    ls = HashStoreLocalFile()
#    ls.init(config)
#    pl = Planner(ls, str(tmp_path))
#    space = ResourceSpace(str(tmp_path))
#    _hash = space.calculate_hash(res)
#    state = State(_hash, res.getId())
#    state.set_result({}, "build")
#    state.set_result({}, "test")
#    state.set_result({}, "publish")
#    state.set_result({}, "deploy")
#    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
#        f.write("a")
#    with pytest.raises(FileNotFoundError):
#        open(os.path.join(tmp_path, "test"), "r")
#    pl.planAction("deploy", res, None, state.get_imdict(), [])
#    with open(os.path.join(tmp_path, "test"), "r") as f:
#        assert f.read() == "a"
#    g = pl.getGraph()
#    assert len(g.getNodes()) == 0


def test_plan_1(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    env_res = create_resource(tmp_path / "resource.env.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("build", res, env_res, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_1.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_1.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_11(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    env_res = create_resource(tmp_path / "resource.env.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("test", res, env_res, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_11.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_11.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_2(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    env_res = create_resource(tmp_path / "resource.env.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    env_state = State(space.calculate_hash(env_res), env_res.getId())
    env_state.set_result({}, "build", "fake")
    ls.store(env_state)
    pl.plan("build", res, env_res, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_2.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_2.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_3(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    res2 = create_resource(res_dir / "resource.fake2.yaml", "Fake", "fake2")
    res = create_resource(
        res_dir / "resource.yaml", "Fake", "fake", depends_on=[{"id": "Fake:fake2"}]
    )
    env_res = create_resource(res_dir / "resource.env.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(res_dir))
    space = ResourceSpace(str(res_dir))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    env_state = State(space.calculate_hash(env_res), env_res.getId())
    env_state.set_result({}, "build", "fake")
    ls.store(env_state)
    pl.plan("build", res, env_res, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_3.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_3.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_4(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.fake2.yaml", "Fake", "fake2")
    res = create_resource(
        res_dir / "resource.yaml",
        "Fake",
        "fake",
        depends_on=[{"id": "Fake:fake2", "action2": "test"}],
    )
    env_res = create_resource(res_dir / "resource.env.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(res_dir))
    space = ResourceSpace(str(res_dir))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    env_state = State(space.calculate_hash(env_res), env_res.getId())
    ls.store(env_state)
    pl.plan("build", res, env_res, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_4.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_4.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_5(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(res_dir / "resource.fake2.yaml", "Fake", "fake2")
    create_resource(res_dir / "resource.fake3.yaml", "Fake", "fake3")
    res = create_resource(
        res_dir / "resource.yaml",
        "Fake",
        "fake",
        depends_on=[
            {"id": "Fake:fake2", "action2": "test"},
            {"id": "Fake:fake3", "action2": "build"},
        ],
    )
    env_res = create_resource(res_dir / "resource.env.yaml", "Env", "fake")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(res_dir))
    space = ResourceSpace(str(res_dir))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    env_state = State(space.calculate_hash(env_res), env_res.getId())
    ls.store(env_state)
    pl.plan("build", res, env_res, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_5.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_5.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_6(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    create_resource(
        res_dir / "resource.fake2.yaml",
        "Fake",
        "fake2",
        depends_on=[{"id": "Fake:fake3", "action2": "build"}],
    )
    create_resource(res_dir / "resource.fake3.yaml", "Fake", "fake3")
    res = create_resource(
        res_dir / "resource.yaml",
        "Fake",
        "fake",
        depends_on=[{"id": "Fake:fake2", "action2": "test"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(res_dir))
    space = ResourceSpace(str(res_dir))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("build", res, None, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_6.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_6.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_7(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    res = create_resource(
        res_dir / "resource.shared.yaml",
        "Fake",
        "A",
        depends_on=[{"id": "Fake:B", "action1": "test", "action2": "deploy"}],
    )
    create_resource(res_dir / "resource.doks.yaml", "Fake", "B")
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(res_dir))
    space = ResourceSpace(str(res_dir))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("deploy", res, None, state.get_imdict())
    g = pl.getGraph()
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_7.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_7.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])


def test_plan_8(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    env_res = create_resource(res_dir / "resource.dev.yaml", "Env", "dev")
    res = create_resource(
        res_dir / "resource.yaml", "Fake", "res", spec={"depends_on_env": False}
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(res_dir))
    space = ResourceSpace(str(res_dir))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("deploy", res, env_res, state.get_imdict())
    g = pl.getGraph()
    assert g.getNodes()[0].getMetadata("action_reason") == "new deps"
    g2, _ = read_graph(os.path.join(base_path, "graphs/test_plan_8.graph"))
    assert g == g2
    a = g.walk_number()
    output_file = os.path.join(base_path, "graphs/test_plan_8.output")
    with open(output_file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            assert lines[i].strip() == str(a[i])
