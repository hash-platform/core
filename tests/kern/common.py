from pathlib import PosixPath
import yaml

from hash.resources import get_resource

def create_resource_file(file, kind="", name="", spec="", parent="", depends_on = [], envs = [], remote = {}, env = ""):
    data = {
        "apiVersion": "hash.io/v1alpha1",
        "metadata": {}
    }
    if kind:
        data["kind"] = kind
    if name:
        data["metadata"]["name"] = name
    if parent:
        data["metadata"]["parent"] = parent
    if depends_on:
        data["metadata"]["depends_on"] = depends_on
    if envs:
        data["metadata"]["envs"] = envs
    if env:
        data["metadata"]["env"] = env
    if spec:
        data["spec"] = spec
    if remote:
        data["metadata"]["remote"] = remote
    with open(file, "w+") as f:
        f.write(yaml.dump(data))


def create_resource(file, kind="", name="", spec="", parent="", depends_on = [], envs = [], remote = {}, env = ""):
    create_resource_file(file, kind, name, spec, parent, depends_on, envs, remote, env)
    return get_resource(str(file))

def create_local_file_store_config(output : PosixPath, org = "hashio", project = "hash") -> dict:
    output.mkdir()
    return {
        "output": str(output),
        "organization": org,
        "project": project
    }
