import pytest

import os
from hash.kern.templates import Renderer
from hash.store.base import HashStoreLocalFile
from hash.kern.state import State

from hash.resources import ResourceSpace

from common import create_resource, create_local_file_store_config
from hash.utils import Artifact


def test_get_hash_templates_empty(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    r = Renderer(res, sp, st, {})
    f = r.get_hash_templates()
    assert len(f) == 0


def test_get_hash_templates1(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    r = Renderer(res, sp, st, {})
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("a")
    f = r.get_hash_templates()
    assert len(f) == 1
    assert f[0] == "./test.hash"


def test_get_hash_templates2(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    r = Renderer(res, sp, st, {})
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("a")
    os.mkdir(str(tmp_path) + "/test")
    with open(os.path.join(tmp_path, "test", "test.hash"), "w") as f:
        f.write("a")
    f = r.get_hash_templates()
    assert len(f) == 1
    assert f[0] == "./test.hash"


def test_get_envs(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"x": 1}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"no_env": {"build": {"y": 2}}}}
    r = Renderer(res, sp, st, globs)
    envs = r.get_envs()
    assert envs["x"] == 1
    assert envs["globals"].get("Fake:fake", "build", "y") == 2


def test_render1(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    r = Renderer(res, sp, st, {})
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("a")
    r.render("x")
    with open(os.path.join(tmp_path, "test"), "r") as f:
        assert f.read().strip() == "a"


def test_render2(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"no_env": {"build": {"y": 2}}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("{{ globals.get('Fake:fake', 'build', 'y') }}")
    r.render()
    with open(os.path.join(tmp_path, "test"), "r") as f:
        assert f.read().strip() == "2"


def test_render3(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"e1": "v1"}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"build": {"y": 2}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("v is {{ e1 }}")
    r.render()
    with open(os.path.join(tmp_path, "test"), "r") as f:
        assert f.read().strip() == "v is v1"


def test_render4(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake")
    with open(os.path.join(res_dir, "test.hash"), "w") as f:
        f.write("{{ artifacts.get_artifact('Fake:fake', 'build', 'test').getData() }}")
    sp = ResourceSpace(str(res_dir))
    h = sp.calculate_hash(res)
    st = State(h, "Fake:fake")
    st.set_result(
        {"artifacts": [Artifact("test", "build", None, h, "text", "hello")]}, "build"
    )
    ls.store(st)
    r = Renderer(res, sp, ls, {})
    r.render()
    with open(os.path.join(res_dir, "test"), "r") as f:
        assert f.read().strip() == "hello"


def test_render5(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake")
    sp = ResourceSpace(str(tmp_path))
    with open(os.path.join(res_dir, "test.hash"), "w") as f:
        f.write("{{ artifacts.get_artifact('Fake:fake', 'build', 'test').getData() }}")
    st = State(sp.calculate_hash(res), "Fake:fake")
    st.set_result(
        {"artifacts": [Artifact("test", "build", "dev", "a", "text", "hello")]},
        "build",
        "dev",
    )
    ls.store(st)
    r = Renderer(res, sp, ls, {}, "dev")
    r.render()
    with open(os.path.join(res_dir, "test"), "r") as f:
        assert f.read().strip() == "hello"


def test_clear1(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"e1": "v1"}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"build": {"y": 2}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("v is {{ e1 }}")
    r.render()
    with open(os.path.join(tmp_path, "test"), "r") as f:
        assert f.read().strip() == "v is v1"
    r.clear()
    with pytest.raises(FileNotFoundError):
        open(os.path.join(tmp_path, "test"), "r")


def test_clear2(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"e1": "v1"}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"build": {"y": 2}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("v is {{ e1 }}")
    r.render()
    with open(os.path.join(tmp_path, "test"), "r") as f:
        assert f.read().strip() == "v is v1"
    os.remove(os.path.join(tmp_path, "test"))
    with pytest.raises(FileNotFoundError):
        open(os.path.join(tmp_path, "test"), "r")
    r.clear()
    with pytest.raises(FileNotFoundError):
        open(os.path.join(tmp_path, "test"), "r")


def test_get_deps_empty(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"e1": "v1"}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"build": {"y": 2}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("{{ e1 }}")
    deps = r.get_deps()
    assert len(deps) == 0


def test_get_deps1(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"e1": "v1"}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"build": {"y": 2}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("{{ globals.get('Fake:fake', 'build', 'y') }}")
    deps = r.get_deps()
    assert len(deps) == 1
    assert deps[0]["id"] == "Fake:fake"
    assert deps[0]["action2"] == "build"


def test_get_deps2(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"e1": "v1"}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"build": {"y": 2}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("{{ globals.get('Fake:fake', 'build', 'y') }}")
    with open(os.path.join(tmp_path, "test2.hash"), "w") as f:
        f.write("{{ globals.get('Fake:fake', 'publish', 'y') }}")
    deps = r.get_deps()
    assert len(deps) == 2
    assert deps[0]["id"] == "Fake:fake"
    # assert deps[0]["action2"] == "publish"
    assert deps[1]["id"] == "Fake:fake"
    # assert deps[1]["action2"] == "build"


def test_get_deps3(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"envs": {"e1": "v1"}}
    )
    st = State("a", "Fake:fake")
    ls.store(st)
    sp = ResourceSpace(str(tmp_path))
    globs = {"Fake:fake": {"build": {"y": 2}}}
    r = Renderer(res, sp, st, globs)
    with open(os.path.join(tmp_path, "test.hash"), "w") as f:
        f.write("{{ globals.get('Fake:fake', 'build', 'y') }}")
    with open(os.path.join(tmp_path, "test2.hash"), "w") as f:
        f.write("{{ globals.get('Fake:fake', 'publish', 'y') }}")
    with open(os.path.join(tmp_path, "test3.hash"), "w") as f:
        f.write("{{ import x }}")
    deps = r.get_deps()
    assert len(deps) == 2
    assert deps[0]["id"] == "Fake:fake"
    # assert deps[0]["action2"] == "publish"
    assert deps[1]["id"] == "Fake:fake"
    # assert deps[1]["action2"] == "build"
