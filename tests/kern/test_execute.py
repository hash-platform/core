import os
from hash.kern.planner import Planner
from hash.kern.state import State
from hash import dag
from hash.kern.execute import Execute
from hash.resources.base import ResourceSpace
from hash.store.base import HashStoreLocalFile
from common import create_resource, create_local_file_store_config

# Read a graph from a file


def read_graph(file):
    d = dag.Graph()
    ret_nodes = []
    with open(file, "r") as f:
        line = f.readline()
        while line:
            nodes = line.split(" ")
            if len(nodes) != 2:
                continue
            n1 = dag.Node(nodes[0])
            if n1 not in ret_nodes:
                ret_nodes.append(n1)
            other_nodes = nodes[1].split(",")
            for node in other_nodes:
                other_node = dag.Node(node.strip())
                if other_node not in ret_nodes:
                    ret_nodes.append(other_node)
                e = dag.Edge(n1, other_node)
                d.addEdge(e)
            line = f.readline()
    return d, ret_nodes


base_path = os.path.dirname(__file__)


def test_build_1_resource(tmp_path):
    spec = {
        "pre_build_command": "echo -n hash > pre.build.txt",
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("build", res, None, state.get_imdict())
    g = pl.getGraph()
    e = Execute(space, ls, "SerialExecutor", {})
    e.execute(g, {})
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "hash"


def test_test_1_resource(tmp_path):
    spec = {
        "pre_build_command": "echo -n hash > pre.build.txt",
        "pre_test_command": "echo -n hash > pre.test.txt",
        "build_command": "echo -n hash > build.txt",
        "test_command": "echo -n hash > test.txt",
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("test", res, None, state.get_imdict())
    g = pl.getGraph()
    e = Execute(space, ls, "SerialExecutor", {})
    e.execute(g, {})
    with open(tmp_path / "pre.test.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "build.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "test.txt") as f:
        assert f.read() == "hash"


def test_publish_1_resource(tmp_path):
    spec = {
        "pre_build_command": "echo -n hash > pre.build.txt",
        "pre_test_command": "echo -n hash > pre.test.txt",
        "pre_publish_command": "echo -n hash > pre.publish.txt",
        "build_command": "echo -n hash > build.txt",
        "test_command": "echo -n hash > test.txt",
        "publish_command": "echo -n hash > publish.txt",
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("publish", res, None, state.get_imdict())
    g = pl.getGraph()
    e = Execute(space, ls, "SerialExecutor", {})
    e.execute(g, {})
    with open(tmp_path / "pre.test.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "pre.publish.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "pre.build.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "build.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "test.txt") as f:
        assert f.read() == "hash"
    with open(tmp_path / "publish.txt") as f:
        assert f.read() == "hash"


def test_build_2_resources(tmp_path):
    spec1 = {
        "build_command": "echo -n hash1 > build1.txt",
    }
    spec2 = {
        "build_command": "echo -n hash2 > build2.txt",
    }
    create_resource(tmp_path / "resource.yaml", "Fake", "fake1", spec1)
    res = create_resource(
        tmp_path / "resource2.yaml",
        "Fake",
        "fake",
        spec2,
        depends_on=[{"id": "Fake:fake1"}],
    )
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    pl = Planner(ls, str(tmp_path))
    space = ResourceSpace(str(tmp_path))
    _hash = space.calculate_hash(res)
    state = State(_hash, res.getId())
    pl.plan("build", res, None, state.get_imdict())
    g = pl.getGraph()
    e = Execute(space, ls, "SerialExecutor", {})
    e.execute(g, {})
    with open(tmp_path / "build2.txt") as f:
        assert f.read() == "hash2"
    with open(tmp_path / "build1.txt") as f:
        assert f.read() == "hash1"
