import json
import pytest
from hash import errors, utils
from hash.kern import State


def test_wrong_hash():
    with pytest.raises(errors.StateError):
        State(1, "1")


def test_wrong_resource_id():
    with pytest.raises(errors.StateError):
        State("12", 1)


def test_wrong_resource_id_state():
    with pytest.raises(errors.StateError):
        State(False, 4)


def test_simple_state():
    st = State("asd", "xfg")
    assert st.get_hash() == "asd"
    assert st.get_resource_id() == "xfg"
    st.__hash = "ert"  # You should not be able to change the hash
    assert st.get_hash() == "asd"


def test_set_result():
    result = {"x": 9, "i": "asd"}
    st = State("a", "b")
    st.set_result(result, "build")
    assert st.get_result("build")["x"] == 9
    result["x"] = 1
    assert st.get_result("build")["x"] == 9


def test_set_result_with_env():
    result = {"x": 9, "i": "asd"}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    assert st.get_result("build", "dev")["x"] == 9
    result["x"] = 1
    assert st.get_result("build", "dev")["x"] == 9


def test_set_result_wrong():
    result = 9
    st = State("a", "b")
    with pytest.raises(errors.StateError):
        st.set_result(result, "build")


def test_set_result_wrong_env():
    st = State("a", "b")
    with pytest.raises(errors.StateError):
        st.set_result({}, "build", {})


def test_set_result_wrong_action():
    st = State("a", "b")
    with pytest.raises(errors.StateError):
        st.set_result({}, 1)


def test_set_result_env():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    assert st.get_result("build", "dev")["x"] == 1


def test_set_result_env_actions():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    st.set_result(result, "test", "dev")
    assert st.get_result("build", "dev")["x"] == 1
    assert st.get_result("test", "dev")["x"] == 1


def test_get_result_wrong_action():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    with pytest.raises(errors.StateError):
        st.get_result(1)


def test_get_result_wrong_env():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    with pytest.raises(errors.StateError):
        st.get_result("a", 2)


def test_get_globals_wrong_action_type():
    result = {"globals": {"x": 1}}
    st = State("a", "b")
    st.set_result(result, "build")
    with pytest.raises(errors.StateError):
        st.get_globals(1)


def test_get_globals_wrong_env_type():
    result = {"globals": {"x": 1}}
    st = State("a", "b")
    st.set_result(result, "build")
    with pytest.raises(errors.StateError):
        st.get_globals("build", 4)


def test_get_globals():
    result = {"globals": {"x": 1}}
    st = State("a", "b")
    st.set_result(result, "build")
    globs = st.get_globals("build")
    assert type(globs) == utils.ImDict
    assert globs["build"]["x"] == 1


def test_get_globals_env():
    result = {"globals": {"x": 1}}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    globs = st.get_globals("build", "dev")
    assert globs["build"]["x"] == 1
    globs = st.get_globals("build")
    assert globs["build"] == utils.ImDict({})


def test_get_globals_env_all_actions():
    result = {"globals": {"x": 1}}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    globs = st.get_globals("", "dev")
    assert globs["build"]["x"] == 1
    globs["test"] == utils.ImDict({})
    globs["publish"] == utils.ImDict({})
    globs["deploy"] == utils.ImDict({})
    globs = st.get_globals("")
    assert globs["build"] == utils.ImDict({})
    globs["test"] == utils.ImDict({})
    globs["publish"] == utils.ImDict({})
    globs["deploy"] == utils.ImDict({})


def test_get_globals_no_env_all_actions():
    result = {"globals": {"x": 1}}
    st = State("a", "b")
    st.set_result(result, "build")
    globs = st.get_globals("", "dev")
    assert globs["build"] == utils.ImDict({})
    globs["test"] == utils.ImDict({})
    globs["publish"] == utils.ImDict({})
    globs["deploy"] == utils.ImDict({})
    globs = st.get_globals("")
    assert globs["build"]["x"] == 1
    globs["test"] == utils.ImDict({})
    globs["publish"] == utils.ImDict({})
    globs["deploy"] == utils.ImDict({})


def test_get_envs():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    st.set_result(result, "build", "prod")
    envs = st.get_envs()
    assert envs["dev"]["build"]["x"] == 1
    assert envs["prod"]["build"]["x"] == 1


def test_get_envs_change():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    st.set_result(result, "build", "prod")
    envs = st.get_envs()
    assert envs["dev"]["build"]["x"] == 1
    assert envs["prod"]["build"]["x"] == 1


def test_get_env_names_action():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    st.set_result(result, "build", "prod")
    st.set_result(result, "test", "dev")
    envs = st.get_env_names("build")
    assert len(envs) == 2
    assert envs[0] == "dev"
    assert envs[1] == "prod"
    envs = st.get_env_names("test")
    assert len(envs) == 1
    assert envs[0] == "dev"
    envs = st.get_env_names("deploy")
    assert len(envs) == 0


def test_get_env_names_all_actions():
    result = {"x": 1}
    st = State("a", "b")
    st.set_result(result, "build", "dev")
    st.set_result(result, "build", "prod")
    st.set_result(result, "test", "dev")
    envs = st.get_env_names("")
    assert len(envs) == 2
    assert envs[0] == "dev"
    assert envs[1] == "prod"


def test_add_deps_error1():
    st = State("a", "b")
    with pytest.raises(errors.StateError):
        st.add_dep(1, "a")


def test_add_deps_error2():
    st = State("a", "b")
    with pytest.raises(errors.StateError):
        st.add_dep("1", False)


def test_add_deps():
    st = State("a", "b")
    st.add_dep("a", "qw")
    deps = st.get_deps()
    assert deps["a"] == "qw"


def test_get_dep():
    st = State("a", "b")
    st.add_dep("a", "qw")
    dep = st.get_dep("a")
    assert dep == "qw"
    a = st.get_dep("rt")
    assert a is None


def test_get_dep_error():
    st = State("a", "b")
    st.add_dep("a", "qw")
    with pytest.raises(errors.StateError):
        st.get_dep(1)


def test_set_result_artifacts_error1():
    result = {"x": 1, "artifacts": "art1"}
    st = State("a", "b")
    with pytest.raises(errors.StateArtifactError):
        st.set_result(result, "build", "dev")


def test_set_result_artifacts_error2():
    result = {"x": 1, "artifacts": ["art1"]}
    st = State("a", "b")
    with pytest.raises(errors.StateArtifactError):
        st.set_result(result, "build", "dev")


def test_set_result_artifacts_error3():
    art = utils.Artifact("id", "test", "dev", "h", "url", "data")
    result = {"x": 1, "artifacts": [art]}
    st = State("a", "b")
    with pytest.raises(errors.StateArtifactError):
        st.set_result(result, "build", "dev")


def test_set_result_artifacts_error4():
    art = utils.Artifact("id", "build", "prod", "h", "url", "data")
    result = {"x": 1, "artifacts": [art]}
    st = State("a", "b")
    with pytest.raises(errors.StateArtifactError):
        st.set_result(result, "build", "dev")


def test_set_result_artifacts():
    art = utils.Artifact("id", "build", "prod", "h", "url", "data")
    result = {"x": 1, "artifacts": [art]}
    st = State("a", "b")
    st.set_result(result, "build", "prod")


def test_get_artifacts():
    art = utils.Artifact("id", "build", "prod", "h", "url", "data")
    result = {"x": 1, "artifacts": [art]}
    st = State("a", "b")
    st.set_result(result, "build", "prod")
    artfs = st.get_artifacts("build", "prod")
    assert len(artfs) == 1
    assert artfs["build"][0].getId() == "id"


def test_get_artifacts_all_actions():
    art = utils.Artifact("id", "build", "prod", "h", "url", "data")
    result = {"x": 1, "artifacts": [art]}
    st = State("a", "b")
    st.set_result(result, "build", "prod")
    af = utils.Artifact("id2", "test", "prod", "h", "url", "data")
    rs = {"artifacts": [af]}
    st.set_result(rs, "test", "prod")
    artfs = st.get_artifacts("", "prod")
    assert len(artfs) == 4
    assert artfs["build"][0].getId() == "id"
    assert artfs["test"][0].getId() == "id2"
    assert len(artfs["publish"]) == 0
    assert len(artfs["deploy"]) == 0


def test_get_json_str1():
    st = State("a", "b")
    st.add_dep("a", "qw")
    rs = {"x": 2}
    st.set_result(rs, "build", "dev")
    js = st.get_json_str()
    jst = json.loads(js)
    assert jst["hash"] == "a"
    assert jst["envs"]["dev"]["build"]["x"] == 2


def test_get_json_str2():
    st = State("a", "b")
    st.add_dep("a", "qw")
    aft = utils.Artifact("id", "build", "dev", "a", "url", "data")
    rs = {"x": 2, "artifacts": [aft]}
    st.set_result(rs, "build", "dev")
    js = st.get_json_str()
    jst = json.loads(js)
    assert jst["hash"] == "a"
    assert jst["envs"]["dev"]["build"]["x"] == 2
    assert jst["envs"]["dev"]["build"]["artifacts"][0]["id"] == "id"


def test_get_json_str3():
    st = State("a", "b")
    st.add_dep("a", "qw")
    aft = utils.Artifact("id", "build", None, "a", "url", "data")
    rs = {"x": 2, "artifacts": [aft]}
    st.set_result(rs, "build")
    js = st.get_json_str()
    jst = json.loads(js)
    assert jst["hash"] == "a"
    assert jst["no_env"]["build"]["x"] == 2
    assert jst["no_env"]["build"]["artifacts"][0]["id"] == "id"


def test_get_json_str_resource_id():
    st = State("a", "b")
    st.add_dep("a", "qw")
    aft = utils.Artifact("id", "build", "dev", "a", "url", "data")
    rs = {"x": 2, "artifacts": [aft]}
    st.set_result(rs, "build", "dev")
    js = st.get_json_str(True)
    jst = json.loads(js)
    assert jst["hash"] == "a"
    assert jst["resource_id"] == "b"
    assert jst["envs"]["dev"]["build"]["x"] == 2
    assert jst["envs"]["dev"]["build"]["artifacts"][0]["id"] == "id"


def test_get_status():
    st = State("a", "b")
    sta = st.get_status()
    assert sta["last_action"] == ""
    assert sta["error"] == ""


def test_set_status():
    st = State("a", "b")
    st.set_status("publish", "test")
    sta = st.get_status()
    assert sta["last_action"] == "publish"
    assert sta["error"] == "test"


def test_read_state_empty():
    state = {}
    with pytest.raises(errors.StateError):
        State.read_state(state)


def test_read_state_wrong_resource_id1():
    state = {}
    with pytest.raises(errors.StateError):
        State.read_state(state, 1)


def test_read_state_wrong_resource_id2():
    state = {"resource_id": 2}
    with pytest.raises(errors.StateError):
        State.read_state(state)


def test_read_state_no_hash():
    state = {}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_wrong_hash():
    state = {"hash": 3}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_ok1():
    state = {"hash": "h"}
    st = State.read_state(state, "a")
    assert st.get_hash() == "h"
    assert st.get_resource_id() == "a"


def test_read_state_ok2():
    state = {"hash": "h", "resource_id": "r"}
    st = State.read_state(state, "a")
    assert st.get_hash() == "h"
    assert st.get_resource_id() == "r"


def test_read_state_ok3():
    state = {"hash": "h", "resource_id": "r"}
    st = State.read_state(state)
    assert st.get_hash() == "h"
    assert st.get_resource_id() == "r"


def test_read_state_wrong_status1():
    state = {"hash": "h", "status": "a"}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_wrong_status2():
    state = {"hash": "h", "status": {"last_action": 2}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_wrong_status3():
    state = {"hash": "h", "status": {"last_action": "2", "error": 4}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_status_error1():
    state = {"hash": "h", "status": {"last_action": "2", "error": "4"}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_status_error2():
    state = {"hash": "h", "status": {"last_action": "build", "error": "4"}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_status_ok():
    state = {"hash": "h", "status": {"last_action": "test", "error": "build"}}
    st = State.read_state(state, "a")
    st.get_status()["last_action"] == "test"
    st.get_status()["error"] == "build"


def test_read_state_wrong_deps1():
    state = {"hash": "h", "deps": []}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_wrong_deps2():
    state = {"hash": "h", "deps": {1: "a"}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_wrong_deps3():
    state = {"hash": "h", "deps": {"a": 1}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_deps_ok():
    state = {"hash": "h", "deps": {"a": "b"}}
    st = State.read_state(state, "a")
    st.get_dep("a") == "b"


def test_read_state_envs_error1():
    state = {"hash": "h", "envs": "a"}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_envs_error2():
    state = {"hash": "h", "envs": {1: 9}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_envs_error3():
    state = {"hash": "h", "envs": {"build": 9}}
    with pytest.raises(errors.StateError):
        State.read_state(state, "a")


def test_read_state_envs_ok1():
    state = {"hash": "h", "envs": {"dev": {}}}
    st = State.read_state(state, "a")
    rs = st.get_result("build", "dev")
    assert rs == utils.ImDict({})


def test_read_state_envs_ok2():
    state = {"hash": "h", "envs": {"dev": {"build": {"x": 1}}}}
    st = State.read_state(state, "a")
    rs = st.get_result("build", "dev")
    assert rs["x"] == 1


def test_last_error_empty():
    state = {"hash": "a"}
    st = State.read_state(state, "a")
    assert st.last_error() == ""


def test_last_error_build():
    state = {"hash": "a", "status": {"error": "build"}}
    st = State.read_state(state, "a")
    assert st.last_error() == "build"


def test_last_action_empty():
    state = {"hash": "a"}
    st = State.read_state(state, "a")
    assert st.last_action() == ""


def test_last_action_build():
    state = {"hash": "a", "status": {"last_action": "build"}}
    st = State.read_state(state, "a")
    assert st.last_action() == "build"


def test_state_change_env():
    state = {"hash": "a", "envs": {"dev": {"build": {"x": {"a": 1}}}}}
    st = State.read_state(state, "b")
    envs = st.get_envs()
    assert envs["dev"]["build"]["x"]["a"] == 1
    with pytest.raises(errors.DictError):
        envs["dev"]["build"]["x"]["a"] = 2
    es = st.get_envs()
    assert es["dev"]["build"]["x"]["a"] == 1


def test_state_change_env_result():
    state = {"hash": "a", "envs": {"dev": {"build": {"x": {"a": 1}}}}}
    st = State.read_state(state, "b")
    envs = st.get_envs()
    assert envs["dev"]["build"]["x"]["a"] == 1
    st.set_result({"y": 2}, "build", "dev")
    assert envs["dev"]["build"]["y"] == 2


def test_state_change_no_env():
    state = {"hash": "a", "no_env": {"build": {"x": {"a": 1}}}}
    st = State.read_state(state, "b")
    no_env = st.get_no_env()
    assert no_env["build"]["x"]["a"] == 1
    with pytest.raises(errors.DictError):
        no_env["build"]["x"]["a"] = 2
    es = st.get_no_env()
    assert es["build"]["x"]["a"] == 1


def test_state_change_no_env_result():
    state = {"hash": "a", "no_env": {"build": {"x": {"a": 1}}}}
    st = State.read_state(state, "b")
    no_env = st.get_no_env()
    assert no_env["build"]["x"]["a"] == 1
    st.set_result({"y": 2}, "build")
    assert no_env["build"]["y"] == 2


def test_state_change_result():
    state = {"hash": "a", "envs": {"dev": {"build": {"x": 12}}}}
    st = State.read_state(state, "b")
    rs = st.get_result("build", "dev")
    assert rs["x"] == 12
    with pytest.raises(errors.DictError):
        rs["x"] = 13
    assert rs["x"] == 12
    st.set_result({"x": 14}, "build", "dev")
    assert rs["x"] == 12


def test_get_state_imdict():
    state = {
        "hash": "1",
        "envs": {"dev": {"build": {"v": 901}}},
        "no_env": {"build": {"a": 90}},
        "deps": {"id": "hash"},
        "status": {"last_action": "test", "error": "publish"},
        "local": {"dev": {"build": {"x": 1}}},
    }
    st = State.read_state(state, "b")
    dt = st.get_imdict()
    assert type(dt) == utils.ImDict
    assert dt["envs"]["dev"]["build"]["v"] == 901
    with pytest.raises(errors.DictError):
        dt["envs"]["t"] = {"build": {"a": 0}}
    assert dt["resource_id"] == "b"
    assert dt["hash"] == "1"
    assert dt["status"]["last_action"] == "test"
    assert dt["deps"]["id"] == "hash"
    st.set_result({"a": 1}, "test", "dev")
    assert dt["envs"]["dev"]["test"]["a"] == 1
    assert dt["local"]["dev"]["build"]["x"] == 1


def test_get_result_local():
    state = {"hash": "a", "local": {"dev": {"build": {"x": 12}}}}
    st = State.read_state(state, "b")
    rs = st.get_result_local("build", "dev")
    assert rs["x"] == 12


def test_get_result_local_no_env():
    state = {"hash": "a", "local": {"dev": {"build": {"x": 12}}}}
    st = State.read_state(state, "b")
    st.set_result_local({"x": 1}, "build")
    rs = st.get_result_local("build")
    assert rs["x"] == 1


def test_set_result_local():
    st = State("h", "r")
    st.set_result_local({"x": 1}, "build")
    rs = st.get_result_local("build")
    assert rs["x"] == 1
