import json
import os
from pathlib import PosixPath

import pytest
from hash import errors
from hash.kern.state import State
from hash.store.base import HashStoreLocalFile
from hash.utils import Artifact


def create_local_file_store_config(
    output: PosixPath, org="hashio", project="hash"
) -> dict:
    output.mkdir()
    return {"output": str(output), "organization": org, "project": project}


def test_local_file_store_globals(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    ls.store_globals({"x": "y"})
    with open(output_dir / "hashio" / "hash" / "globals.json") as f:
        assert json.loads(f.read())["x"] == "y"


def test_local_get_globals(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    globals_dir = output_dir / "hashio" / "hash"
    with open(globals_dir / "globals.json", "w") as f:
        d = {"x": "y"}
        json.dump(d, f)
    globs = ls.get_globals()
    assert globs["x"] == "y"


def test_init_no_org():
    config = {}
    with pytest.raises(errors.ConfigStoreError):
        HashStoreLocalFile().init(config)


def test_init_wrong_org():
    config = {"organization": 1}
    with pytest.raises(errors.ConfigStoreError):
        HashStoreLocalFile().init(config)


def test_init_no_project():
    config = {"organization": "a"}
    with pytest.raises(errors.ConfigStoreError):
        HashStoreLocalFile().init(config)


def test_init_wrong_project():
    config = {"organization": "a", "project": [3]}
    with pytest.raises(errors.ConfigStoreError):
        HashStoreLocalFile().init(config)


def test_init_no_output():
    config = {"organization": "a", "project": "aa"}
    with pytest.raises(errors.ConfigStoreError):
        HashStoreLocalFile().init(config)


def test_init_wrong_output():
    config = {"organization": "a", "project": "aa", "output": 4}
    with pytest.raises(errors.ConfigStoreError):
        HashStoreLocalFile().init(config)


def test_init_not_existing_output(tmp_path):
    config = {"organization": "a", "project": "aa", "output": str(tmp_path / "no")}
    with pytest.raises(errors.ConfigStoreError):
        HashStoreLocalFile().init(config)


def test_init_existing_output(tmp_path):
    output = tmp_path / "output" / "a" / "aa"
    os.makedirs(output)
    config = {"organization": "a", "project": "aa", "output": str(tmp_path / "output")}
    HashStoreLocalFile().init(config)


def test_local_store_artifact(tmp_path: PosixPath):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    aft_file = tmp_path / "aft.txt"
    with open(aft_file, "w") as f:
        f.write("xyz")
    aft = Artifact("id", "build", "e", "h", "file", str(aft_file))
    ls.store_artifact(aft, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / aft.file_name()) as f:
        assert f.read() == "xyz"


def test_local_store_artifact_path_exists(tmp_path: PosixPath):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    aft_file = tmp_path / "aft.txt"
    with open(aft_file, "w") as f:
        f.write("xyz")
    aft = Artifact("id", "build", "e", "h", "file", str(aft_file))
    (output_dir / "hashio" / "hash" / "r_id").mkdir()
    ls.store_artifact(aft, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / aft.file_name()) as f:
        assert f.read() == "xyz"


def test_local_store_state(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = State("a", "b")
    ls.store_state(json.dumps({"a": state.get_json_str()}), "b")
    with open(output_dir / "hashio" / "hash" / "b" / "hash.json", "r") as f:
        assert json.loads(json.load(f)["a"])["hash"] == "a"


def test_local_get_states(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = {"a": {"hash": "a"}}
    os.makedirs(output_dir / "hashio" / "hash" / "b")
    with open(output_dir / "hashio" / "hash" / "b" / "hash.json", "w") as f:
        json.dump(state, f)
    states = ls.get_states("b")
    assert states["a"]["hash"] == "a"


def test_local_store_store_state(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = State("h", "a")
    ls.store(state)
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"


def test_local_store_save_state(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = State("h", "a")
    state.set_result({"a": "y"}, "build")
    ls.save(state)
    st = ls.get("a", "h")
    assert st.get_result("build")["a"] == "y"


def test_local_store_save_state_globals(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = State("h", "a")
    state.set_result({"globals": {"y": "a"}}, "build")
    ls.save(state)
    assert ls.read_globals()["a"]["no_env"]["build"]["y"] == "a"


def test_local_store_store_multiple_states(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = State("h", "a")
    ls.store(state)
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"
    state2 = State("h2", "a")
    state2.set_result({"x": "y"}, "build")
    ls.store(state2)
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h2"], "a")
        assert st.get_hash() == "h2"
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"


def test_local_store_store_multiple_resources(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = State("h", "a")
    ls.store(state)
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"
    state2 = State("h2", "b")
    state2.set_result({"x": "y"}, "build")
    ls.store(state2)
    with open(output_dir / "hashio" / "hash" / "b" / "hash.json") as f:
        st = State.read_state(json.load(f)["h2"], "a")
        assert st.get_hash() == "h2"
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"


def test_local_store_store_multiple_resources_states(tmp_path):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    state = State("h", "a")
    ls.store(state)
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"
    state2 = State("h2", "b")
    state2.set_result({"x": "y"}, "build")
    ls.store(state2)
    with open(output_dir / "hashio" / "hash" / "b" / "hash.json") as f:
        st = State.read_state(json.load(f)["h2"], "a")
        assert st.get_hash() == "h2"
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"
    state3 = State("h3", "a")
    state3.set_result({"x": "y"}, "build", "env")
    ls.store(state3)
    with open(output_dir / "hashio" / "hash" / "b" / "hash.json") as f:
        st = State.read_state(json.load(f)["h2"], "a")
        assert st.get_hash() == "h2"
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h"], "a")
        assert st.get_hash() == "h"
    with open(output_dir / "hashio" / "hash" / "a" / "hash.json") as f:
        st = State.read_state(json.load(f)["h3"], "a")
        assert st.get_hash() == "h3"
        assert st.get_result("build", "env")["x"] == "y"


def test_local_store_artifact_path_first(tmp_path: PosixPath):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    aft_file = tmp_path / "aft.txt"
    with open(aft_file, "w") as f:
        f.write("xyz")
    aft = Artifact("id", "build", "e", "h", "file", str(aft_file), "relative_path")
    ls.store_artifact(aft, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / aft.file_name()) as f:
        assert f.read() == "xyz"
    with open(output_dir / "hashio" / "hash" / "r_id" / "artifacts.path") as f:
        paths = f.readlines()
        assert len(paths) == 1
        assert paths[0].strip() == "relative_path"


def test_local_store_artifact_path_second(tmp_path: PosixPath):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    aft_file = tmp_path / "aft.txt"
    with open(aft_file, "w") as f:
        f.write("xyz")
    aft = Artifact("id", "build", "e", "h", "file", str(aft_file), "relative_path")
    ls.store_artifact(aft, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / aft.file_name()) as f:
        assert f.read() == "xyz"
    with open(output_dir / "hashio" / "hash" / "r_id" / "artifacts.path") as f:
        paths = f.readlines()
        assert len(paths) == 1
        assert paths[0].strip() == "relative_path"
    ls.store_artifact(aft, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / "artifacts.path") as f:
        paths = f.readlines()
        assert len(paths) == 1
        assert paths[0].strip() == "relative_path"


def test_local_store_artifact_path_two(tmp_path: PosixPath):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    aft_file = tmp_path / "aft.txt"
    with open(aft_file, "w") as f:
        f.write("xyz")
    aft_file2 = tmp_path / "aft2.txt"
    with open(aft_file2, "w") as f:
        f.write("xyz")
    aft = Artifact("id", "build", "e", "h", "file", str(aft_file), "relative_path")
    aft2 = Artifact("id2", "build", "e", "h", "file", str(aft_file2), "relative_path2")
    ls.store_artifact(aft, "r_id")
    ls.store_artifact(aft2, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / aft.file_name()) as f:
        assert f.read() == "xyz"
    with open(output_dir / "hashio" / "hash" / "r_id" / "artifacts.path") as f:
        paths = f.readlines()
        assert len(paths) == 2
        assert paths[0].strip() == "relative_path"
        assert paths[1].strip() == "relative_path2"
    ls.store_artifact(aft, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / "artifacts.path") as f:
        paths = f.readlines()
        assert len(paths) == 2
        assert paths[0].strip() == "relative_path"
        assert paths[1].strip() == "relative_path2"


def test_local_store_artifact_paths(tmp_path: PosixPath):
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    aft_file = tmp_path / "aft.txt"
    with open(aft_file, "w") as f:
        f.write("xyz")
    aft_file2 = tmp_path / "aft2.txt"
    with open(aft_file2, "w") as f:
        f.write("xyz")
    aft = Artifact("id", "build", "e", "h", "file", str(aft_file), "relative_path")
    aft2 = Artifact("id2", "build", "e", "h", "file", str(aft_file2), "relative_path2")
    ls.store_artifact(aft, "r_id")
    ls.store_artifact(aft2, "r_id")
    with open(output_dir / "hashio" / "hash" / "r_id" / aft.file_name()) as f:
        assert f.read() == "xyz"
    paths = ls.artifact_paths("r_id")
    assert len(paths) == 2
    assert paths[0].strip() == "relative_path"
    assert paths[1].strip() == "relative_path2"
