import json
import os
import pytest
from hash.store import HashStore

from hash import errors

from hash.kern import State

from hash.utils import Artifact

# Make sure to empty the global, artifacts, and state before running any test


@pytest.fixture(autouse=True)
def run_around_tests():
    global gloabs
    global artifacts
    global states
    gloabs = {}
    artifacts = {}
    states = {}
    # A test function will be run at this point
    yield


def test_init_base_str_config():
    hs = HashStore()
    with pytest.raises(errors.ConfigStoreError):
        hs._init("")


def test_init_base_no_org():
    hs = HashStore()
    with pytest.raises(errors.ConfigStoreError):
        hs._init({})


def test_init_base_wrong_org():
    hs = HashStore()
    with pytest.raises(errors.ConfigStoreError):
        hs._init({"organization": 1})


def test_init_base_no_project():
    hs = HashStore()
    with pytest.raises(errors.ConfigStoreError):
        hs._init({"organization": "a"})


def test_init_base_wrong_project():
    hs = HashStore()
    with pytest.raises(errors.ConfigStoreError):
        hs._init({"organization": "a", "project": False})


def test_init_base():
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    assert hs.get_organization() == "a"
    assert hs.get_project() == "p"


def test_store_wrong_state():
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    with pytest.raises(errors.StateStoreError):
        hs.store({})


gloabs = {}
artifacts = {}
states = {}


def store_state(state, resource_id):
    global states
    states[resource_id] = state


def get_states(resource_id):
    global states
    return states.get(resource_id, {})


def get_globals():
    global gloabs
    return gloabs


def store_globals(g):
    global gloabs
    gloabs = g


def store_artifact(artifact, resource_id):
    if resource_id == "drop":
        raise Exception()
    global artifacts
    artfs = artifacts.get(resource_id)
    if artfs:
        artifacts[resource_id].append(artifact)
    else:
        artifacts[resource_id] = [artifact]
    if resource_id == "remove":
        os.remove(artifact.getData())


def test_store_artifacts_wrong_state():
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    with pytest.raises(errors.StateStoreError):
        hs.store_artifacts({})


def test_store_artifacts():
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    state = State("a", "b")
    state.set_result(
        {"artifacts": [Artifact("id", "build", "e", "a", "text", "d")]}, "build", "e"
    )
    hs.store_artifacts(state)


def test_store_artifacts_file1(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    state = State("a", "b")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    state.set_result({"artifacts": [artifact]}, "build", "e")
    hs.store_artifact = store_artifact
    assert hs.store_artifacts(state) is None
    assert len(artifacts["b"]) == 1
    assert artifacts["b"][0] == artifact


def test_store_artifacts_file2(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    artf_file1 = tmp_path / "test1.txt"
    artf_file1.write_text("asd1")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    state = State("a", "b")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    artifact1 = Artifact("id", "build", "e", "a", "file", str(artf_file1))
    state.set_result({"artifacts": [artifact, artifact1]}, "build", "e")
    hs.store_artifact = store_artifact
    assert hs.store_artifacts(state) is None
    assert len(artifacts["b"]) == 2
    assert artifacts["b"][0] == artifact
    assert artifacts["b"][1] == artifact1


def test_store_artifacts_file3(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    artf_file1 = tmp_path / "test1.txt"
    artf_file1.write_text("asd1")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    state = State("a", "b")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    artifact1 = Artifact("id", "test", "e", "a", "file", str(artf_file1))
    state.set_result({"artifacts": [artifact]}, "build", "e")
    state.set_result({"artifacts": [artifact1]}, "test", "e")
    hs.store_artifact = store_artifact
    assert hs.store_artifacts(state) is None
    assert len(artifacts["b"]) == 2
    assert artifacts["b"][0] == artifact
    assert artifacts["b"][1] == artifact1


def test_store_artifacts_file4(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    artf_file1 = tmp_path / "test1.txt"
    artf_file1.write_text("asd1")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    state = State("a", "b")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    artifact1 = Artifact("id", "test", None, "a", "file", str(artf_file1))
    state.set_result({"artifacts": [artifact]}, "build", "e")
    state.set_result({"artifacts": [artifact1]}, "test")
    hs.store_artifact = store_artifact
    assert hs.store_artifacts(state) is None
    assert len(artifacts["b"]) == 2
    assert artifacts["b"][0] == artifact
    assert artifacts["b"][1] == artifact1


def test_store_artifacts_file_error(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    state = State("a", "drop")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    state.set_result({"artifacts": [artifact]}, "build", "e")
    hs.store_artifact = store_artifact
    with pytest.raises(Exception):
        e = hs.store_artifacts(state)
        if e is not None:
            raise e


def test_store_artifacts_file_removed(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    state = State("a", "remove")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    state.set_result({"artifacts": [artifact]}, "build", "e")
    hs.store_artifact = store_artifact
    assert hs.store_artifacts(state) is None
    assert len(artifacts["remove"]) == 1
    assert artifacts["remove"][0] == artifact


def test_store_state_with_global1():
    state = State("a", "b")
    r = {"globals": {"x": 1, "y": [4, 1], "z": {"a": 2}}}
    state.set_result(r, "build")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    hs.get_globals = get_globals
    hs.store_globals = store_globals
    hs.store_state = store_state
    hs.store(state)
    st = State.read_state(json.loads(states["b"])["a"], "b")
    assert st.get_result("build")["globals"]["x"] == 1
    assert st.get_result("build")["globals"]["y"] == (4, 1)
    assert st.get_result("build")["globals"]["z"]["a"] == 2


def test_store_state_with_global2():
    state = State("a", "b")
    r = {"globals": {"x": 1, "y": [4, 1], "z": {"a": 2}}}
    state.set_result(r, "build")
    r2 = {"globals": {"r2": "second"}}
    state.set_result(r2, "test", "e")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    hs.get_globals = get_globals
    hs.store_globals = store_globals
    hs.store_state = store_state
    hs.store(state)
    st = State.read_state(json.loads(states["b"])["a"], "b")
    assert st.get_result("build")["globals"]["x"] == 1
    assert st.get_result("build")["globals"]["y"] == (4, 1)
    assert st.get_result("build")["globals"]["z"]["a"] == 2
    assert st.get_result("test", "e")["globals"]["r2"] == "second"


def test_store_state_with_global3():
    state = State("a", "b")
    r = {"globals": {"x": 1, "y": [4, 1], "z": {"a": 2}}}
    state.set_result(r, "build")
    r2 = {"globals": {"r2": "second"}}
    state.set_result(r2, "test", "e")
    r3 = {"globals": {"x": 451}}
    state.set_result(r3, "publish")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    hs.get_globals = get_globals
    hs.store_globals = store_globals
    hs.store_state = store_state
    hs.store(state)
    print(states)
    st = State.read_state(json.loads(states["b"])["a"], "b")
    assert st.get_result("build")["globals"]["x"] == 1
    assert st.get_result("build")["globals"]["y"] == (4, 1)
    assert st.get_result("build")["globals"]["z"]["a"] == 2
    assert st.get_result("test", "e")["globals"]["r2"] == "second"
    assert st.get_result("publish")["globals"]["x"] == 451


def test_store_state_with_artifact_error(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    state = State("a", "drop")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    r = {"artifacts": [artifact]}
    state.set_result(r, "build", "e")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    hs.get_globals = get_globals
    hs.store_globals = store_globals
    hs.store_state = store_state
    hs.store_artifact = store_artifact
    with pytest.raises(Exception):
        hs.store(state)


def test_store_state_with_artifact(tmp_path):
    artf_file = tmp_path / "test.txt"
    artf_file.write_text("asd")
    print(artf_file)
    state = State("a", "s")
    artifact = Artifact("id", "build", "e", "a", "file", str(artf_file))
    r = {"artifacts": [artifact]}
    state.set_result(r, "build", "e")
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    hs.get_globals = get_globals
    hs.store_globals = store_globals
    hs.store_state = store_state
    hs.store_artifact = store_artifact
    hs.store(state)
    # TODO: The store_state function here does not actually store the artifacts anywhere
    # assert states["s"]["a"].get_result("build", "e")["artifacts"][0] == artifact


def test_get_state():
    state = State("a", "b")
    r = {"globals": {"x": "1"}, "ty": "a"}
    state.set_result(r, "build")
    states["b"] = {"a": state}
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    st = hs.get("b", "a")
    assert st.get_result("build")["ty"] == "a"


def test_get_state_dict():
    state = State("a", "b")
    r = {"globals": {"x": "1"}, "ty": "a"}
    state.set_result(r, "build")
    states["b"] = {"a": json.loads(state.get_json_str())}
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    st = hs.get("b", "a")
    assert st.get_result("build")["ty"] == "a"


def test_get_state_error():
    states["b"] = {"a": 1}
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    with pytest.raises(errors.StateStoreError):
        hs.get("b", "a")


def test_get_state_artifact():
    artifact = Artifact("id", "build", None, "a", "text", "hello hash")
    r = {"artifacts": [artifact]}
    state = State("a", "b")
    state.set_result(r, "build")
    states["b"] = {"a": state.get_json_str()}
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    st = hs.get("b", "a")
    assert st.get_result("build")["artifacts"][0] == artifact


def test_get_state_2_hash():
    state = State("a", "b")
    state.set_result({}, "build")
    states["b"] = {"a": state.get_json_str()}
    state2 = State("c", "b")
    state2.set_result({}, "build")
    states["b"]["c"] = state2.get_json_str()
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    st = hs.get("b", "a")
    assert st.get_hash() == "a"
    st2 = hs.get("b", "c")
    assert st2.get_hash() == "c"


def test_get_state_no_hash():
    state = State("a", "b")
    state.set_result({}, "build")
    states["b"] = {"a": state.get_json_str()}
    state2 = State("c", "b")
    state2.set_result({}, "build")
    states["b"]["c"] = state2.get_json_str()
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    assert hs.get("b", "d").get_hash() == "d"


def test_get_state_no_resource():
    state = State("a", "b")
    state.set_result({}, "build")
    states["b"] = {"a": state.get_json_str()}
    state2 = State("c", "b")
    state2.set_result({}, "build")
    states["b"]["c"] = state2.get_json_str()
    hs = HashStore()
    hs._init({"organization": "a", "project": "p"})
    hs.get_states = get_states
    assert hs.get("asf", "d").get_resource_id() == "asf"
