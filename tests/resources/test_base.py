import pytest
import yaml
from hash import errors, utils, kern
from hash.resources import Resource, get_resource
from common import create_resource_file, create_resource
from hash.resources.base import FakeResource, ResourceSpace
from hash.store.base import HashStoreLocalFile
from tests.store.test_local_store import create_local_file_store_config


def test_resource_wrong_arg():
    with pytest.raises(FileNotFoundError):
        Resource("a")


def test_resource_simple(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    assert res.getPath() == str(tmp_path) + "/."


def test_get_resource_int():
    with pytest.raises(FileNotFoundError):
        get_resource(9)


def test_get_resource_no_file():
    with pytest.raises(FileNotFoundError):
        get_resource("9")


def test_get_resource_no_kind(tmp_path):
    d = {}
    f = tmp_path / "resource.yaml"
    f.write_text(yaml.dump(d))
    with pytest.raises(errors.ResourceError):
        get_resource(str(f))


def test_get_resource_no_metadata(tmp_path):
    d = {"kind": "Fake"}
    f = tmp_path / "resource.yaml"
    f.write_text(yaml.dump(d))
    with pytest.raises(errors.ResourceError):
        get_resource(str(f))


def test_get_resource_wrong_metadata(tmp_path):
    d = {"kind": "Fake", "metadata": "a"}
    f = tmp_path / "resource.yaml"
    f.write_text(yaml.dump(d))
    with pytest.raises(errors.ResourceError):
        get_resource(str(f))


def test_get_resource_no_name(tmp_path):
    d = {"kind": "Fake", "metadata": {"x": "a"}}
    f = tmp_path / "resource.yaml"
    f.write_text(yaml.dump(d))
    with pytest.raises(errors.ResourceError):
        get_resource(str(f))


def test_get_resource_no_yaml(tmp_path):
    f = tmp_path / "resource.yaml"
    f.write_text("a")
    with pytest.raises(errors.ResourceError):
        get_resource(str(f))


def test_get_resource_invalid_yaml(tmp_path):
    f = tmp_path / "resource.yaml"
    f.write_text(
        """
    a:r
    y:
       - ajhsa
    """
    )
    with pytest.raises(errors.ResourceInvalidYaml):
        get_resource(str(f))


def test_get_resource_invalid_yaml2(tmp_path):
    f = tmp_path / "resource.yaml"
    d = {"path": tmp_path}
    f.write_text(yaml.dump(d))
    with pytest.raises(errors.ResourceInvalidYaml):
        get_resource(str(f))


def test_get_resource_simple(tmp_path):
    d = {"kind": "Fake", "metadata": {"name": "fake"}}
    f = tmp_path / "resource.yaml"
    f.write_text(yaml.dump(d))
    r = get_resource(str(f))
    assert type(r) == FakeResource
    assert r.getKind() == "Fake"
    assert r.getName() == "fake"


def test_fake_call_action_wrong(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    with pytest.raises(errors.ResourceError):
        res.action("fake", utils.ImDict({}), "a")


def test_fake_call_build(tmp_path):
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"build_result": "asd"}
    )
    env_res = create_resource(tmp_path / "resource.yaml", "Env", "fake")
    ret = res.action("build", utils.ImDict({}), env_res)
    assert type(ret) == dict
    assert ret["build_result"] == "asd"


def test_fake_call_test(tmp_path):
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"test_result": "asd"}
    )
    env_res = create_resource(tmp_path / "resource.yaml", "Env", "fake")
    ret = res.action("test", utils.ImDict({}), env_res)
    assert type(ret) == dict
    assert ret["test_result"] == "asd"


def test_fake_call_publish(tmp_path):
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"publish_result": "asd"}
    )
    env_res = create_resource(tmp_path / "resource.yaml", "Env", "fake")
    ret = res.action("publish", utils.ImDict({}), env_res)
    assert type(ret) == dict
    assert ret["publish_result"] == "asd"


def test_fake_call_deploy(tmp_path):
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", {"deploy_result": "asd"}
    )
    env_res = create_resource(tmp_path / "resource.yaml", "Env", "fake")
    ret = res.action("deploy", utils.ImDict({}), env_res)
    assert type(ret) == dict
    assert ret["deploy_result"] == "asd"


def test_resource_space(tmp_path):
    d = {"kind": "Fake", "metadata": {"name": "fake"}}
    f = tmp_path / "resource.yaml"
    f.write_text(yaml.dump(d))
    rs = ResourceSpace(tmp_path)
    r = rs.read(str(f))
    assert type(r) == FakeResource
    assert r.getKind() == "Fake"
    assert r.getName() == "fake"


def test_find_resource1(tmp_path):
    create_resource_file(tmp_path / "resource.yaml", "Fake", "fake")
    rs = ResourceSpace(tmp_path)
    r = rs.find_resource("fake", "Fake")
    assert r.getName() == "fake"
    assert r.getKind() == "Fake"


def test_find_resource2(tmp_path):
    create_resource_file(tmp_path / "resource.yaml", "Fake", "fake")
    create_resource_file(tmp_path / "resource.fake2.yaml", "Fake", "fake2")
    rs = ResourceSpace(tmp_path)
    r = rs.find_resource("fake", "Fake")
    assert r.getName() == "fake"
    assert r.getKind() == "Fake"
    r = rs.find_resource("fake2", "Fake")
    assert r.getName() == "fake2"
    assert r.getKind() == "Fake"
    r = rs.find_resource("fake2", "Fake2")
    assert r is None


def test_find_resource3(tmp_path):
    d = tmp_path / "dir1"
    d.mkdir()
    create_resource_file(tmp_path / "resource.yaml", "Fake", "fake")
    create_resource_file(tmp_path / "resource.fake2.yaml", "Fake", "fake2")
    create_resource_file(d / "resource.yaml", "Fake", "fake3")
    rs = ResourceSpace(tmp_path)
    r = rs.find_resource("fake", "Fake")
    assert r.getName() == "fake"
    assert r.getKind() == "Fake"
    r = rs.find_resource("fake2", "Fake")
    assert r.getName() == "fake2"
    assert r.getKind() == "Fake"
    r = rs.find_resource("fake3", "Fake")
    assert r.getName() == "fake3"
    assert r.getKind() == "Fake"


def test_mutate1(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {"x": "1"})
    assert res.getSpec("x") == "1"
    res.mutate({"x": 2})
    assert res.getSpec("x") == 2


def test_mutate2(tmp_path):
    res_spec = {"x": {"a": 1, "b": 2}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    assert res.getSpec("x")["a"] == 1
    assert res.getSpec("x")["b"] == 2
    m_spec = {"x": {"a": 3, "c": 3}}
    res.mutate(m_spec)
    assert res.getSpec("x").get("a") == 1
    assert res.getSpec("x").get("c") == 3
    assert res.getSpec("x").get("b") == 2


def test_mutate3(tmp_path):
    res_spec = {"x": {"a": 1, "b": 2}, "l": [1, 3]}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    assert res.getSpec("x")["a"] == 1
    assert res.getSpec("x")["b"] == 2
    assert res.getSpec("l")[0] == 1
    assert res.getSpec("l")[1] == 3
    m_spec = {"x": {"a": 3, "c": 3}, "l": [1, 2]}
    res.mutate(m_spec)
    assert res.getSpec("x").get("a") == 1
    assert res.getSpec("x").get("c") == 3
    assert res.getSpec("x").get("b") == 2
    assert res.getSpec("l")[0] == 1
    assert res.getSpec("l")[1] == 3
    assert res.getSpec("l")[2] == 1
    assert res.getSpec("l")[3] == 2


def test_mutate4(tmp_path):
    res_spec = {"x": {"a": 1, "b": 2, "d": {"x": 1}}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    assert res.getSpec("x")["a"] == 1
    assert res.getSpec("x")["b"] == 2
    assert res.getSpec("x")["d"]["x"] == 1
    m_spec = {"x": {"a": 3, "c": 3, "d": {"y": 2}}}
    res.mutate(m_spec)
    assert res.getSpec("x").get("a") == 1
    assert res.getSpec("x").get("c") == 3
    assert res.getSpec("x").get("b") == 2
    assert res.getSpec("x")["d"]["x"] == 1
    # assert res.getSpec("x")["d"]["y"] == 2 #TODO: Fix the mutate spec so it can go deeper in the specs update


def test_get_id(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {}, "Fake:fake2")
    assert res.getId() == "Fake:fake"
    assert res.getParentId() == "Fake:fake2"


def test_get_invalid_parent_id(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {}, "Fake:fake2:")
    with pytest.raises(errors.ResourceError):
        res.getParentId()
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {}, "Fake:")
    with pytest.raises(errors.ResourceError):
        res.getParentId()
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {}, ":fake")
    with pytest.raises(errors.ResourceError):
        res.getParentId()
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {}, ":")
    with pytest.raises(errors.ResourceError):
        res.getParentId()
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", {})
    assert res.getParentId() is None


def test_find_resource_by_id_error(tmp_path):
    rs = ResourceSpace(str(tmp_path))
    with pytest.raises(errors.ResourceError):
        rs.find_resource_by_id("a:b:")


def test_fill_specs1(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": 1}}}
    assert res.getSpec("x") == "$Fake:fake.build.x"
    res.fill_specs(None, _globals, rs)
    assert res.getSpec("x") == 1


def test_fill_specs1(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": 1}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs2(tmp_path):
    res_spec = {"x": "$Fake:fak.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": 1}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs3(tmp_path):
    res_spec = {"x": "$Fake:fake.buil.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": 1}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs4(tmp_path):
    res_spec = {"x": "$Fake:fake1.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake1": {"build": {"x": 1}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs5(tmp_path):
    res_spec = {"x": "$Fake:fake.build.y"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": 1}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs6(tmp_path):
    res_spec = {"x": "$Fake:fake.build.y"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": 1}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs7(tmp_path):
    res_spec = {"x": "$Fake:fake.build.y"}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", res_spec, env="dev"
    )
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": 1}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs8(tmp_path):
    res_spec = {"x": "$Fake:fake.build.y"}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", res_spec, env="dev"
    )
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"dev": {"x": 1}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs9(tmp_path):
    res_spec = {"x": "$Fake:fake.build.y"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"dev": {"x": 1}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs10(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"x": 1}}}}
    res.fill_specs("dev", _globals, rs)
    assert res.getSpec("x") == 1


def test_fill_specs11(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x"}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", res_spec, env="dev"
    )
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"x": 1}}}}
    res.fill_specs(None, _globals, rs)
    assert res.getSpec("x") == 1


def test_fill_specs12(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": 1}}}}
    res.fill_specs(None, _globals, rs)
    assert res.getSpec("x") == 1


def test_fill_specs13(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x.y"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": {"y": 1}}}}}
    res.fill_specs(None, _globals, rs)
    assert res.getSpec("x") == 1


def test_fill_specs14(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x.y"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"x": {"build": {"z": 1}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs15(tmp_path):
    res_spec = {"x": "$Fake:fake.build.x.y.a"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": {"y": {"a": "aaa"}}}}}}
    res.fill_specs(None, _globals, rs)
    assert res.getSpec("x") == "aaa"


def test_fill_specs16(tmp_path):
    res_spec = {"$Fake:fake.build.x": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": "a"}}}}
    res.fill_specs(None, _globals, rs)
    assert res.getSpec("a") == "1"


def test_fill_specs17(tmp_path):
    res_spec = {"$Fake:fak.build.x": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": "a"}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs18(tmp_path):
    res_spec = {"$Fake:fake1.build.x": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake1": {"no_env": {"build": {"x": "a"}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs19(tmp_path):
    res_spec = {"$Fake:fake.buil.x": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": "a"}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs20(tmp_path):
    res_spec = {"$Fake:fake.buil.y": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": "a"}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs21(tmp_path):
    res_spec = {"$Fake:fake.build.x": "1"}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", res_spec, env="dev"
    )
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": "a"}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs22(tmp_path):
    res_spec = {"$Fake:fake.build.x": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"build": {"x": "a"}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs23(tmp_path):
    res_spec = {"$Fake:fake.build.x": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"y": "a"}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs("dev", _globals, rs)


def test_fill_specs24(tmp_path):
    res_spec = {"$Fake:fake.build.x": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"y": "a"}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs25(tmp_path):
    res_spec = {"$Fake:fake.build.x.y": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": {"a": 1}}}}}
    with pytest.raises(errors.ResourceSpecError):
        res.fill_specs(None, _globals, rs)


def test_fill_specs26(tmp_path):
    res_spec = {"$Fake:fake.build.x.y": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"no_env": {"build": {"x": {"y": 1}}}}}
    res.fill_specs(None, _globals, rs)
    assert res.getSpec(1) == "1"


def test_fill_specs27(tmp_path):
    res_spec = {"$Fake:fake.build.x.y": "1"}
    res = create_resource(
        tmp_path / "resource.yaml", "Fake", "fake", res_spec, env="dev"
    )
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"x": {"y": 1}}}}}
    res.fill_specs(None, _globals, rs)
    assert res.getSpec(1) == "1"


def test_fill_specs28(tmp_path):
    res_spec = {"$Fake:fake.build.x.y": "1"}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"x": {"y": 1}}}}}
    res.fill_specs("dev", _globals, rs)
    assert res.getSpec(1) == "1"


def test_fill_specs29(tmp_path):
    res_spec = {"x": {"a": "$Fake:fake.build.x.y"}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"x": {"y": 1}}}}}
    res.fill_specs("dev", _globals, rs)
    assert res.getSpec("x")["a"] == 1


def test_fill_specs30(tmp_path):
    res_spec = {"x": {"a": {"z": "$Fake:fake.build.x.y"}}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"x": {"y": 1}}}}}
    res.fill_specs("dev", _globals, rs)
    assert res.getSpec("x")["a"]["z"] == 1


def test_fill_specs31(tmp_path):
    res_spec = {"x": {"a": [{"z": "$Fake:fake.build.x.y"}]}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    rs = ResourceSpace(tmp_path)
    _globals = {"Fake:fake": {"dev": {"build": {"x": {"y": 1}}}}}
    res.fill_specs("dev", _globals, rs)
    assert res.getSpec("x")["a"][0]["z"] == 1


def test_cal_spec1(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    rs = ResourceSpace(tmp_path)
    rs.cal_spec(res, "dev", {})


def test_cal_spec2(tmp_path):
    res_spec = {"x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    create_resource(tmp_path / "resource.yaml", "Env", "dev")
    rs = ResourceSpace(tmp_path)
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1


def test_cal_spec3(tmp_path):
    res_spec = {"x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    m_spec = {"Fake": {"y": 2}}
    create_resource(tmp_path / "resource.yaml", "Env", "dev", m_spec)
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("y") is None
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1
    assert res.getSpec("y") == 2


def test_cal_spec4(tmp_path):
    res_spec = {"x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    m_spec = {"Fake-fake": {"y": 2}}
    create_resource(tmp_path / "resource.yaml", "Env", "dev", m_spec)
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("y") is None
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1
    assert res.getSpec("y") == 2


def test_cal_spec5(tmp_path):
    res_spec = {"x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    m_spec = {"Fake-fake": {"y": 2}, "Fake": {"y": 3}}
    create_resource(tmp_path / "resource.yaml", "Env", "dev", m_spec)
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("y") is None
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1
    assert res.getSpec("y") == 2


def test_cal_spec6(tmp_path):
    res_spec = {"x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    m_spec = {"Fake-fake": {"y": 2}, "Fake": {"z": 3}}
    create_resource(tmp_path / "resource.yaml", "Env", "dev", m_spec)
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("y") is None
    assert res.getSpec("z") is None
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1
    assert res.getSpec("y") == 2
    assert res.getSpec("z") == 3


def test_cal_spec7(tmp_path):
    res_spec = {"x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    m_spec = {"Fake-fake": {"y": 2}, "Fake": {"z": 3, "path": "asd"}}
    create_resource(tmp_path / "resource.yaml", "Env", "dev", m_spec)
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("y") is None
    assert res.getSpec("z") is None
    assert res.getSpec("path") is None
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1
    assert res.getSpec("y") == 2
    assert res.getSpec("z") == 3
    assert res.getSpec("path") is None


def test_cal_spec8(tmp_path):
    res_spec = {"x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    m_spec = {"Fake-fake": {"y": 2, "dont": "8"}, "Fake": {"z": 3, "path": "asd"}}
    create_resource(tmp_path / "resource.yaml", "Env", "dev", m_spec)
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("y") is None
    assert res.getSpec("z") is None
    assert res.getSpec("path") is None
    assert res.getSpec("dont") is None
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1
    assert res.getSpec("y") == 2
    assert res.getSpec("z") == 3
    assert res.getSpec("path") is None
    assert res.getSpec("dont") is None


def test_cal_spec91(tmp_path):
    res_spec = {"x": 1, "dont": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    m_spec = {"Fake-fake": {"y": 2, "dont": "8"}, "Fake": {"z": 3, "path": "asd"}}
    create_resource(tmp_path / "resource.yaml", "Env", "dev", m_spec)
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("y") is None
    assert res.getSpec("z") is None
    assert res.getSpec("path") is None
    assert res.getSpec("dont") is 1
    rs.cal_spec(res, "dev", {})
    assert res.getSpec("x") == 1
    assert res.getSpec("y") == 2
    assert res.getSpec("z") == 3
    assert res.getSpec("path") is None
    assert res.getSpec("dont") is 1


def test_cal_spec9(tmp_path):
    res_spec = {"x": 1, "$Fake:fake.build.x": 1}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", res_spec)
    create_resource(tmp_path / "resource.dev.yaml", "Env", "dev")
    rs = ResourceSpace(tmp_path)
    assert res.getSpec("x") == 1
    assert res.getSpec("$Fake:fake.build.x") == 1
    _globals = {"Fake:fake": {"no_env": {"build": {"x": "a"}}}}
    rs.cal_spec(res, None, _globals)
    assert res.getSpec("x") == 1
    assert res.getSpec("$Fake:fake.build.x") is None
    assert res.getSpec("a") == 1


def test_fake_artifact(tmp_path):
    spec = {
        "artifacts": [
            {
                "id": "af1",
                "kind": "file",
                "content": "af test",
                "path": str(tmp_path / "af.txt"),
            }
        ]
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    env = create_resource(tmp_path / "resource.dev.yaml", "Env", "fake")
    result = res.action("build", utils.ImDict({"hash": "asd"}), env)
    assert len(result.get("artifacts")) == 1
    with open(str(tmp_path / "af.txt"), "r") as f:
        assert f.read() == "af test"


def test_fake_artifact2(tmp_path):
    spec = {
        "artifacts": [{"id": "af1", "kind": "file", "path": str(tmp_path / "af.txt")}]
    }
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    env = create_resource(tmp_path / "resource.dev.yaml", "Env", "fake")
    with open(str(tmp_path / "af.txt"), "w") as f:
        f.write("af test")
    result = res.action("build", utils.ImDict({"hash": "asd"}), env)
    assert len(result.get("artifacts")) == 1
    with open(result.get("artifacts")[0].getData(), "r") as f:
        assert f.read() == "af test"
    assert result.get("artifacts")[0].getId() == "af1"
    assert result.get("artifacts")[0].getKind() == "file"
    assert result.get("artifacts")[0].getAction() == "build"
    assert result.get("artifacts")[0].getEnv() == "fake"
    assert result.get("artifacts")[0].getHash() == "asd"


def test_fake_artifact3(tmp_path):
    spec = {"artifacts": [{"id": "af1", "kind": "url", "data": "https://hash.io"}]}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    env = create_resource(tmp_path / "resource.dev.yaml", "Env", "fake")
    result = res.action("build", utils.ImDict({"hash": "asd"}), env)
    assert result.get("artifacts")[0].getData() == "https://hash.io"


def test_fake_artifact4(tmp_path):
    spec = {"artifacts": [{"id": "af1", "kind": "text", "data": "hash io"}]}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    env = create_resource(tmp_path / "resource.dev.yaml", "Env", "fake")
    result = res.action("build", utils.ImDict({"hash": "asd"}), env)
    assert result.get("artifacts")[0].getData() == "hash io"


def test_calculate_hash_ignore_artifacts(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    spec = {
        "artifacts": [
            {
                "id": "af1",
                "kind": "file",
                "content": "af test",
                "path": str(res_dir / "af.txt"),
            }
        ]
    }
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    rs = ResourceSpace(str(tmp_path), ls)
    h = rs.calculate_hash(res)
    result = res.action("build", utils.ImDict({"hash": h}), None)
    st = kern.State(h, res.getId())
    st.set_result(result, "build")
    assert len(result.get("artifacts")) == 1
    with open(str(res_dir / "af.txt"), "r") as f:
        assert f.read() == "af test"
    ls.store(st)
    with open(str(res_dir / "af.txt"), "w") as f:
        f.write("asd")
    assert h == rs.calculate_hash(res)


def test_calculate_hash_ignore_templates(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", {})
    rs = ResourceSpace(str(tmp_path))
    with open(res_dir / "test.txt.hash", "w") as f:
        f.write("test template")
    h = rs.calculate_hash(res)
    with open(res_dir / "test.txt", "w") as f:
        f.write("test template")
    assert h == rs.calculate_hash(res)


def test_calculate_hash_ignore_artifacts_templates(tmp_path):
    res_dir = tmp_path / "res"
    res_dir.mkdir()
    spec = {
        "artifacts": [
            {
                "id": "af1",
                "kind": "file",
                "content": "af test",
                "path": str(res_dir / "af.txt"),
            }
        ]
    }
    output_dir = tmp_path / "output"
    config = create_local_file_store_config(output_dir)
    ls = HashStoreLocalFile()
    ls.init(config)
    res = create_resource(res_dir / "resource.yaml", "Fake", "fake", spec)
    rs = ResourceSpace(str(tmp_path), ls)
    with open(res_dir / "test.txt.hash", "w") as f:
        f.write("template")
    h = rs.calculate_hash(res)
    result = res.action("build", utils.ImDict({"hash": h}), None)
    st = kern.State(h, res.getId())
    st.set_result(result, "build")
    assert len(result.get("artifacts")) == 1
    with open(str(res_dir / "af.txt"), "r") as f:
        assert f.read() == "af test"
    ls.store(st)
    with open(str(res_dir / "af.txt"), "w") as f:
        f.write("asd")
    with open(res_dir / "test.txt", "w") as f:
        f.write("template")
    assert h == rs.calculate_hash(res)


def test_getSpecDict1(tmp_path):
    spec = {"x": "4", "z": 2, "l": [1, 2, 3], "d": {"x": 3}}
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    d = res.getSpecDict()
    assert type(d) == dict
    assert d["x"] == "4"
    assert d["z"] == 2
    assert type(d["l"]) == tuple
    assert d["l"][0] == 1
    assert len(d["l"]) == 3
    assert type(d["d"]) == utils.ImDict
    assert d["d"]["x"] == 3


def test_getFile(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    assert res.getFile() == str(tmp_path / "resource.yaml")


def test_calculate_hash(tmp_path):
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake")
    space = ResourceSpace(str(tmp_path))
    _h = kern.Hash().hash(str(tmp_path))
    assert space.calculate_hash(res) == _h


def test_calculate_hash_match(tmp_path):
    spec = {"match": ["test.txt"]}
    test_file = tmp_path / "test.txt"
    test_file.write_text("asd")
    test_file = tmp_path / "test2.txt"
    test_file.write_text("asda")
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    space = ResourceSpace(str(tmp_path))
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml", "test.txt"])
    assert space.calculate_hash(res) == _h
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml"])
    assert space.calculate_hash(res) != _h
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml", "test2.txt"])
    assert space.calculate_hash(res) != _h


def test_calculate_hash_match_build_script(tmp_path):
    spec = {"match": ["test.txt"], "build_script": "test2.txt"}
    test_file = tmp_path / "test.txt"
    test_file.write_text("asd")
    test_file = tmp_path / "test2.txt"
    test_file.write_text("asda")
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    space = ResourceSpace(str(tmp_path))
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml", "test.txt", "test2.txt"])
    assert space.calculate_hash(res) == _h
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml"])
    assert space.calculate_hash(res) != _h
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml", "test2.txt"])
    assert space.calculate_hash(res) != _h


def test_calculate_hash_build_script(tmp_path):
    spec = {"build_script": "test2.txt"}
    test_file = tmp_path / "test2.txt"
    test_file.write_text("asda")
    res = create_resource(tmp_path / "resource.yaml", "Fake", "fake", spec)
    space = ResourceSpace(str(tmp_path))
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml", "test2.txt"])
    assert space.calculate_hash(res) == _h
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml"])
    assert space.calculate_hash(res) != _h


def test_calculate_hash_env(tmp_path):
    test_file = tmp_path / "test2.txt"
    test_file.write_text("asda")
    res = create_resource(tmp_path / "resource.yaml", "Env", "fake")
    space = ResourceSpace(str(tmp_path))
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml"])
    assert space.calculate_hash(res) == _h
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml", "test2.txt"])
    assert space.calculate_hash(res) != _h


def test_calculate_hash_env_match(tmp_path):
    spec = {"match": ["test2.txt"]}
    test_file = tmp_path / "test2.txt"
    test_file.write_text("asda")
    res = create_resource(tmp_path / "resource.yaml", "Env", "fake", spec)
    space = ResourceSpace(str(tmp_path))
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml", "test2.txt"])
    assert space.calculate_hash(res) == _h
    _h = kern.Hash().hash(str(tmp_path), ["resource.yaml"])
    assert space.calculate_hash(res) != _h
