import pytest_httpserver
import pytest
import os
import platform
import subprocess

from hash.resources.base import FileDownloadVerify
from hash import errors


def test_download_file_version(tmp_path, httpserver: pytest_httpserver.HTTPServer):
    httpserver.expect_request("/foobar/2").respond_with_json({"foo": "bar"})
    url = httpserver.url_for("/foobar/2").replace("2", "{version}")
    fdv = FileDownloadVerify(url)
    fdv.download_file(
        "2",
        "109e5841f3c287c4a1340dd700dbc1ae85d055bbce9872a484a4bc149f023f73",
        os.path.join(str(tmp_path), "test"),
    )


def test_download_file_os_name(tmp_path, httpserver: pytest_httpserver.HTTPServer):
    os_name = platform.system().lower()
    httpserver.expect_request(f"/foobar/{os_name}").respond_with_json({"foo": "bar"})
    url = httpserver.url_for(f"/foobar/{os_name}").replace(os_name, "{os_name}")
    fdv = FileDownloadVerify(url)
    fdv.download_file(
        "2",
        "109e5841f3c287c4a1340dd700dbc1ae85d055bbce9872a484a4bc149f023f73",
        os.path.join(str(tmp_path), "test"),
    )


def test_download_file_cpu_arch(tmp_path, httpserver: pytest_httpserver.HTTPServer):
    cpu_arch = platform.processor().lower()
    httpserver.expect_request(f"/foobar/{cpu_arch}").respond_with_json({"foo": "bar"})
    url = httpserver.url_for(f"/foobar/{cpu_arch}").replace(cpu_arch, "{cpu_arch}")
    fdv = FileDownloadVerify(url)
    fdv.download_file(
        "2",
        "109e5841f3c287c4a1340dd700dbc1ae85d055bbce9872a484a4bc149f023f73",
        os.path.join(str(tmp_path), "test"),
    )


def test_download_file_wrong_checksum(
    tmp_path, httpserver: pytest_httpserver.HTTPServer
):
    httpserver.expect_request("/foobar").respond_with_json({"foo": "bar"})
    print(httpserver.url_for("/foobar"))
    fdv = FileDownloadVerify(httpserver.url_for("/foobar"))
    with pytest.raises(errors.ChecksumError):
        fdv.download_file(
            "",
            "009e5841f3c287c4a1340dd700dbc1ae85d055bbce9872a484a4bc149f023f73",
            os.path.join(str(tmp_path), "test"),
        )


@pytest.mark.parametrize("archive", ["test.zip", "test.tar.gz", "test.tar.xz"])
def test_unpack_archive(tmp_path, archive):
    tests_data_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "testdata"
    )
    fdv = FileDownloadVerify("")
    with pytest.raises(FileNotFoundError):
        open(os.path.join(tmp_path, "test.txt"), "r")
    fdv.unpack_archive(os.path.join(tests_data_dir, archive), str(tmp_path))
    with open(os.path.join(tmp_path, "test.txt"), "r") as f:
        assert f.read().strip() == "hello test"


def test_make_exec(tmp_path):
    tests_data_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "testdata"
    )
    fdv = FileDownloadVerify("")
    tmp_path = str(tmp_path)
    subprocess.run(
        f"cp script.sh {tmp_path}", cwd=tests_data_dir, shell=True, check=True
    )
    p = subprocess.run("./script.sh", cwd=tmp_path, shell=True)
    assert p.returncode == 126
    p = subprocess.run("ls", cwd=tests_data_dir, shell=True)
    fdv.make_exec(os.path.join(tmp_path, "script.sh"))
    p = subprocess.run(
        "./script.sh", cwd=tmp_path, shell=True, check=True, capture_output=True
    )
    assert p.stdout.decode("UTF-8").strip() == "hello test"


def test_prepare(tmp_path, httpserver: pytest_httpserver.HTTPServer):
    httpserver.expect_request("/script").respond_with_data(
        "#!/bin/sh\n echo 'hello test'"
    )
    fdv = FileDownloadVerify(httpserver.url_for("/script"))
    tmp_path = str(tmp_path)
    p = subprocess.run("./script.sh", cwd=tmp_path, shell=True)
    assert p.returncode == 127
    fdv.prepare(
        "",
        "ad4e0a40245f50e69eaf30e0d39192b66846c944c0e81433b693b42466be5a05",
        os.path.join(tmp_path, "script.sh"),
        os.path.join(tmp_path, "script.sh"),
    )
    p = subprocess.run(
        "./script.sh", cwd=tmp_path, shell=True, check=True, capture_output=True
    )
    assert p.stdout.decode("UTF-8").strip() == "hello test"
