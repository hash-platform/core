# Changelog

## unreleased

* Add support for using [govulncheck](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck) for checking go code for vulnerabilities. [#193](https://gitlab.com/hash-platform/hashkern/-/merge_requests/193)

## 0.4.0 on 10-06-2023

* Add documentation for K8S Target. [#184](https://gitlab.com/hash-platform/hashkern/-/merge_requests/184/)
* Add documentation for DockerRegistry Target. [#185](https://gitlab.com/hash-platform/hashkern/-/merge_requests/185)
* Add documentation for Terraform Resource. [#186](https://gitlab.com/hash-platform/hashkern/-/merge_requests/186)
* Add docuemntation for DockerImage, GoMicroService and Kustomize resources. [#187](https://gitlab.com/hash-platform/hashkern/-/merge_requests/187)
* Add a new feature to Kustomize resource so it can delete resources from the cluster if they are deleted from the config, set `remove_deleted_resources` spec to true to enable this, it is disabled by default. [#188](https://gitlab.com/hash-platform/hashkern/-/merge_requests/188)
* Add istio resource, you can find docs [heer](https://hash-kern.readthedocs.io/en/latest/resources_docs.html#istio-resource-docs). [#189](https://gitlab.com/hash-platform/hashkern/-/merge_requests/189)
* Enable DockerImage resource to republish docker images if they are removed from the registry, also add new artifact to the resources output called `image_url_digest`, it contains image URL with digest instead of tag. [#190](https://gitlab.com/hash-platform/hashkern/-/merge_requests/190)

## 0.3.0 on 25-05-2023

* Add `version` command to the CLI. [#166](https://gitlab.com/hash-platform/hashkern/-/merge_requests/166)
* Add `--plan` parameter to only do a plan and save it to a file in graphviz format. [#169](https://gitlab.com/hash-platform/hashkern/-/merge_requests/169)
* Enhance error output for terraform and docker resources. [#171](https://gitlab.com/hash-platform/hashkern/-/merge_requests/171) and [#172](https://gitlab.com/hash-platform/hashkern/-/merge_requests/172)
* Support more versions for go (1.20.3,1.20.2,1.19.8,1.19.7,1.19.6,1.19.5,1.19.4,1.18.9 and 1.18.19), golangci-lint (1.52.2,1.52.1,1.52.0 and 1.51.2) and terraform (1.4.5,1.4.4,1.4.3,1.4.2,1.4.1 and 1.4.0). [#173](https://gitlab.com/hash-platform/hashkern/-/merge_requests/173)
* Enable resource developers to save a state for the resource without the hash, so it can be retrieved without a hash value, it can be
added using `local` keyword in the returned dictionary. [#170](https://gitlab.com/hash-platform/hashkern/-/merge_requests/170)
* Add the ability to install kubectl to the Kustomize resource, a version can be selected using `kubectl_version` and it can be installed
by seting `kubectl_install` to true, we can override the hash using `kubectl_check_sum`. [#174](https://gitlab.com/hash-platform/hashkern/-/merge_requests/174)
* Add secrets provider plugins, which enables hash users and developers to add secrets to their resources. [#175](https://gitlab.com/hash-platform/hashkern/-/merge_requests/175)
* Enable hash users to use a secret in their resource's specs using `secret.` synatx. [#176](https://gitlab.com/hash-platform/hashkern/-/merge_requests/176)
* Add the ability to use [infracost](https://www.infracost.io) for alerting when terraform costs exceed a specific value, you can use `infracost.enabled`
to enable this functionality, `infracost.maximum` to specify maximum allowed cost in US dollars, `infracost.api_key` to add the API key for infracost API, it assumes that infracost CLI is installed. [#177](https://gitlab.com/hash-platform/hashkern/-/merge_requests/177)
* Store sensitive terraform outputs as secrets instead of as globals. [#179](https://gitlab.com/hash-platform/hashkern/-/merge_requests/179)
* Add support for GCP secrets provider. [#181](https://gitlab.com/hash-platform/hashkern/-/merge_requests/181)

## 0.2.1 on 19-03-2023

* Enable resources to declare wether they actually depend on their env or not, this can be done by implementing the method `depends_on_env` and return False if the resource doesn't have an actual dependency on the env. [#149](https://gitlab.com/hash-platform/hashkern/-/merge_requests/149).
* Add the reason for running an action to the output, possible reasons are: `new Hash`, `error status`, `new deps`, `new dep: {id of dep}`, `dep {id of dep} changed`, `new action in this env: {environment}`, and `force re-run from resource`. [#151](https://gitlab.com/hash-platform/hashkern/-/merge_requests/151).
* Preserve file mode when saving artifacts, this ensures that an executable binary saved as artifact will also be executable when read from the state [#152](https://gitlab.com/hash-platform/hashkern/-/merge_requests/152).
* Add Digital Ocean functions target which allows resource developers to deploy to Digital Ocean function service [#161](https://gitlab.com/hash-platform/hashkern/-/merge_requests/161)
* Several imporvements to GoMicroservice resource:
  * Support go version 1.20 [#153](https://gitlab.com/hash-platform/hashkern/-/merge_requests/153)
  * Make sure that a GoMicroservice is re-deployed if its manifests change in the cluster [#154](https://gitlab.com/hash-platform/hashkern/-/merge_requests/154).
  * Disable CGO when compiling go code [#155](https://gitlab.com/hash-platform/hashkern/-/merge_requests/155)
  * Add support for go.mod and go.sum in other directories than the service's directory [#160](https://gitlab.com/hash-platform/hashkern/-/merge_requests/160)
  * Deploy go microservices to Digital Ocean functions using DOFunction target, assign `DOFunction` value to `deploy_target` spec to use DOFunction target for deployment, the default value is `kubernetes` to deploy using K8S target [#162](https://gitlab.com/hash-platform/hashkern/-/merge_requests/162)
* Two improvements to terraform resources
  * Add specs to control terraform version used when running terraform commands, use `terraform_version` spec to specify a value for the terraform version [#156](https://gitlab.com/hash-platform/hashkern/-/merge_requests/156).
  * Make sure that terraform apply is re-run if the actual resources change outside of terraform [#157](https://gitlab.com/hash-platform/hashkern/-/merge_requests/157)
* Add overlays support to Kustomize resource, you can now use `overlay` spec to define the overlay name [#158](https://gitlab.com/hash-platform/hashkern/-/merge_requests/158)
* Render templates when planning actions, so resources that need to read templates when running `re_action` methods can use them [#164](https://gitlab.com/hash-platform/hashkern/-/merge_requests/164)
