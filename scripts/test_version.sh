#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage ${0}: <new version>"
    exit 1
fi

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
SETUP_PY_PATH=${SCRIPTPATH}/../setup.py
NEW_VESRION=${1}

VERSION=$(grep version= "${SETUP_PY_PATH}" | cut -d'"' -f2)

if [ "${VERSION}" != "${NEW_VESRION}" ]; then
    echo "Version mismatch: ${VERSION} does not equal ${NEW_VESRION}"
    exit 2
fi
