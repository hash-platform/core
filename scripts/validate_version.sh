#!/bin/bash

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

SETUP_PY_PATH=${SCRIPTPATH}/../setup.py
INIT_PY_PATH=${SCRIPTPATH}/../src/hash/kern/__init__.py
NEW_VESRION=$(grep __VERSION__ "${INIT_PY_PATH}" | cut -d'"' -f2)

VERSION=$(grep version= "${SETUP_PY_PATH}" | cut -d'"' -f2)

if [ "${VERSION}" != "${NEW_VESRION}" ]; then
    echo "Version mismatch: ${VERSION} does not equal ${NEW_VESRION}"
    exit 2
fi
